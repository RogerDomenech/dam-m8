package org.insbaixcamp.dialer;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.provider.ContactsContract;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //Consntant
    final int PERMIS_TRUCADES = 1 ;
    Intent intent = null;
    //buttons
    Button b1;
    Button b2;
    Button b3;
    Button b4;
    Button b5;
    Button b6;
    Button b7;
    Button b8;
    Button b9;
    Button b0;
    // imageButtons
    ImageButton bCerca;
    ImageButton bTruca;
    ImageButton bReset;
    //view Text
    MyEditText etNumero;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/
        //buttons
        this.b0 = findViewById(R.id.b0);
        b0.setOnClickListener(this);
        this.b1 = findViewById(R.id.b1);
        b1.setOnClickListener(this);
        this.b2 = findViewById(R.id.b2);
        b2.setOnClickListener(this);
        this.b3 = findViewById(R.id.b3);
        b3.setOnClickListener(this);
        this.b4 = findViewById(R.id.b4);
        b4.setOnClickListener(this);
        this.b5 = findViewById(R.id.b5);
        b5.setOnClickListener(this);
        this.b6 = findViewById(R.id.b6);
        b6.setOnClickListener(this);
        this.b7 = findViewById(R.id.b7);
        b7.setOnClickListener(this);
        this.b8 = findViewById(R.id.b8);
        b8.setOnClickListener(this);
        this.b9 = findViewById(R.id.b9);
        b9.setOnClickListener(this);
        //imageButtons
        this.bCerca = findViewById(R.id.bCerca);
        bCerca.setOnClickListener(this);
        this.bTruca = findViewById(R.id.bTruca);
        bTruca.setOnClickListener(this);
        this.bReset = findViewById(R.id.bReset);
        bReset.setOnClickListener(this);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        StringBuilder sbUri = new StringBuilder();
        etNumero = findViewById(R.id.etNumero);
        int requestCode;
        ///Toast.makeText(this,String.valueOf(v.getId()),Toast.LENGTH_SHORT).show();
        // si es un "imageButton", mostrem la llibrea de contacctes
        if (v instanceof ImageButton) {
            // una alte forma de compara a nivel de View
            if (v.getId() == R.id.bCerca) {
                intent = new Intent(this, ConctacsActivity.class);
                requestCode = 1;
                startActivityForResult(intent, requestCode);
            }else if (v == findViewById(R.id.bTruca)) {
                sbUri.append("tel:");
                sbUri.append(etNumero.getText());
                intent = new Intent(Intent.ACTION_CALL,
                        Uri.parse(sbUri.toString()));
                //comprovem si el usuari ens dona permis per tucar
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) !=
                        PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE}, PERMIS_TRUCADES);
                } else {
                    startActivity(intent);
                }
            }else{
                etNumero.setText("");
            }

        }else {
            Toast.makeText(this,"test",Toast.LENGTH_SHORT).show();
            etNumero.append(((Button) v).getText());
        }
    }
    //Quan acabi l'acitvitat que he trucat , emp pasara el codi demanant,
    // el codi resultant  i un Intent com a contenedor de dades

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode==1){
            if (resultCode == RESULT_OK){
                etNumero = findViewById(R.id.etNumero);
                etNumero.setText(data.getExtras().getString("Telèfon"));
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //compovem el codi de premis que esn intteresa
        switch (requestCode){
            case PERMIS_TRUCADES:{
                if (grantResults.length>0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){
                    // ens donem premis per turcar
                    startActivity(intent);
                }else{
                    // fem una altre cosa
                }
            }
        }
    }
}
