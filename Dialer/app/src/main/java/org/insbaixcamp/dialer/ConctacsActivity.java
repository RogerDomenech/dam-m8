package org.insbaixcamp.dialer;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class ConctacsActivity extends AppCompatActivity implements AdapterView.OnItemClickListener {
    final int PERMIS_LECTURA_CONTACTES=2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conctacs);
        if(ContextCompat.checkSelfPermission(
                this,
                Manifest.permission.READ_CONTACTS)!= PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.READ_CONTACTS},
                    PERMIS_LECTURA_CONTACTES);
            // Aqui no tinc permisos, torno al "onRequestPermissionsResults

        }else{
            getContactes();
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        //comprovem el codi de permis que ens interesa
        switch (requestCode){
            case PERMIS_LECTURA_CONTACTES:{
                if(grantResults.length>0 && grantResults[0]== PackageManager.PERMISSION_GRANTED){
                    getContactes();
                }
            }
        }
    }

    public void getContactes(){
        // El metodoe "getContetnResolver()"  es un metode de la classe ContentResolver

        Cursor contacts = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI,
                null, // Select all (*)
                ContactsContract.Contacts.HAS_PHONE_NUMBER+"=?", //where contactes tingin el nunmero
                new String[]{"1"},// si es volges mes paratres {"1","2","3"......}
                ContactsContract.Contacts.DISPLAY_NAME+" asc" /// ordenat per nom de contacte ascendent
        );
        ListView lvContactes = findViewById(R.id.lvContactes);

        SimpleCursorAdapter simpleCursorAdapter = new SimpleCursorAdapter(
                this, //context
                android.R.layout.simple_list_item_1, //layout predefint
                contacts, //cursors
                new String[]{ContactsContract.Contacts.DISPLAY_NAME}, // from
                new int[]{android.R.id.text1}, // to: aquest objecte tambe el crea Android
                1);
        lvContactes.setAdapter(simpleCursorAdapter);
        lvContactes.setOnItemClickListener(this);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        //this equival a getAplicationContext()
        Toast.makeText(getApplicationContext(),String.valueOf(id),Toast.LENGTH_LONG).show();
        canviarAdialer(id);
    }
    public void canviarAdialer(long id){
        List<String> llistaTelefons = new ArrayList<String>();
        Cursor cTelefon = getContentResolver().query(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                null,
                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=?",
                new String[]{String.valueOf(id)},
                null);
        while (cTelefon.moveToNext()){
            llistaTelefons.add(cTelefon.getString(cTelefon.getColumnIndex(
                    ContactsContract.CommonDataKinds.Phone.NUMBER
            )));
        }
        cTelefon.close(); //tancar la conexio al System
        if(llistaTelefons.size()>0){
            Intent dada = new Intent();
            dada.putExtra("telefon",llistaTelefons.get(0).toString());
            setResult(RESULT_OK,dada);
            finish(); // retorna a la activitat criadad
        }
    }
}
