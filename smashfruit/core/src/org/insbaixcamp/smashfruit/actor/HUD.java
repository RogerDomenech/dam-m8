package org.insbaixcamp.smashfruit.actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;

public class HUD extends Group {

    static int score;
    int tempScore;
    public BitmapFont font;
    static int fruitesPerRonda = 1;

    public HUD() {
        this.score = 0;
        this.tempScore = 0;
        fruitesPerRonda =1;
        font = new BitmapFont();
        font.setColor(Color.BLUE);
        generateFont();
    }

    public int getFruitesPerRonda() {
        return fruitesPerRonda;
    }

    public void incrementFruitesPerRonda() {
        fruitesPerRonda++;
    }


    public int getScore() {
        return score;
    }

    public static void addScore(int value) {
        score += value;
    }
    public void removeScore(int value){
        this.score=(score>0)?(this.score-(value*5)):0;
    }


    @Override
    public void act(float delta) {
        super.act(delta);
        if (tempScore < score) {
            tempScore += Math.ceil((double)(score-tempScore)/2);
        }else{
            tempScore -= Math.ceil((double)(tempScore-score)/2);
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        font.draw(batch, "Score: " + tempScore, 100, 600);
        super.draw(batch, parentAlpha);
    }

    public void inicialitzarRondes() {
        fruitesPerRonda = 1;
    }
    public void generateFont(){
        FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("Lobster-Regular.ttf"));
        FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
        parameter.size =32;
        parameter.color = Color.BLACK;
        parameter.borderColor=Color.WHITE;
        parameter.borderWidth=3;
        parameter.shadowColor=Color.DARK_GRAY;
        parameter.shadowOffsetX=2;
        parameter.shadowOffsetY=1;
        font = generator.generateFont(parameter); // font size 12 pixels
        generator.dispose(); // don't forget to dispose to avoid memory leaks!
    }


}