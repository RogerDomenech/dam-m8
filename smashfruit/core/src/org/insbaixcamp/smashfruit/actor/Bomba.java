package org.insbaixcamp.smashfruit.actor;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

public class Bomba extends AbstractActor {

    public Bomba(TextureRegion textureRegion, final Sound sound, final HUD hud) {
        super(textureRegion,sound);


        this.addListener(new InputListener(){
            @Override
            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
                //parem objecte
                clearActions();
                //fem que no sigui clicable
                setTouchable(null);
                //animació d'eliminar objecte
                addAction(Actions.parallel(
                        Actions.fadeOut(1),
                        Actions.scaleTo(2,2,1, Interpolation.pow2),
                        Actions.sequence(Actions.delay(1),Actions.removeActor())
                ));
                //sound de bomba
                sound.play();
                //reiniciem contador
               hud.inicialitzarRondes();
               hud.removeScore(100);

                return super.touchDown(event, x, y, pointer, button);
            }
        });
    }
}
