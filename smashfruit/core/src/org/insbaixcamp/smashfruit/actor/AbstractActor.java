package org.insbaixcamp.smashfruit.actor;

import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class AbstractActor extends Image {
    Sound sound;
    public AbstractActor(TextureRegion textureRegion, Sound sound) {
        super(textureRegion);
        this.sound=sound;
        getMoviment();

    }

    public void getMoviment() {

            int pujada = MathUtils.random(700,900);
            int desplasament = MathUtils.random(-100,100);
            int rotacio = MathUtils.random(300,600);
            int direccio = (MathUtils.random()<0.5f)?-1:1;
            this.setOrigin(this.getWidth()/2,
                    this.getHeight()/2);

            this.setPosition(MathUtils.random(0,1000),-300);

            this.addAction(
                        Actions.sequence(
                                Actions.delay(MathUtils.random()),
                                Actions.moveBy(desplasament,pujada,2, Interpolation.pow2),
                                Actions.moveBy(desplasament,-pujada,2,Interpolation.pow2)
                        ));
            this.addAction(Actions.rotateBy(direccio*rotacio,4));


    }

}
