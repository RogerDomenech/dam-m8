package org.insbaixcamp.smashfruit.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Interpolation;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Action;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.InputListener;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FillViewport;

import org.insbaixcamp.smashfruit.SmashFruit;
import org.insbaixcamp.smashfruit.actor.Bomba;
import org.insbaixcamp.smashfruit.actor.Fruit;
import org.insbaixcamp.smashfruit.actor.HUD;

public class GameScreen implements Screen {
    private SmashFruit game;
    private Texture fruitsTexture;
    private TextureRegion bomba;
    private Array<TextureRegion> fruitsRegions;
    private Sound fruitsSound, newRoundSound, bombaSound;
    private Stage stage;
//    private int fruitsPerRonda;
    private HUD hud;
    private Group actors;


    public GameScreen(SmashFruit smashFruit) {
        this.game = smashFruit;
    }

    @Override
    public void show() {
//        fruitsPerRonda=1;
        //Carregem imatges
        fruitsTexture = new Texture("fruits.png");
        bomba = new TextureRegion(fruitsTexture,0,256,256,256);

        fruitsRegions = new Array<TextureRegion>();
        fruitsRegions.add(new TextureRegion(fruitsTexture,0,0,256,256));
        fruitsRegions.add(new TextureRegion(fruitsTexture,256,0,256,256));
        fruitsRegions.add(new TextureRegion(fruitsTexture,256,256,256,256));

        //Carregem sounds
        fruitsSound = Gdx.audio.newSound(Gdx.files.internal("smash3.mp3"));
        bombaSound = Gdx.audio.newSound(Gdx.files.internal("bomb.mp3"));
        newRoundSound = Gdx.audio.newSound(Gdx.files.internal("newround.mp3"));

        //stage
        stage = new Stage(new FillViewport(1024,720));

        //Captura la entrada estandar
        Gdx.input.setInputProcessor(stage);
        actors = new Group();
        stage.addActor(actors);
        //hud
        hud=new HUD();
        //add actors
        stage.addActor(hud);
       // stage.addActor(getBomba());
        //stage.addActor(getFruit());

        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                for (int i = 0; i <  hud.getFruitesPerRonda(); i++) {
                    Actor actor;
                    //actor = (MathUtils.random()<0.1f)? getBomba():getFruit();
                    actor = (MathUtils.random()<0.1f)? new Bomba(bomba,bombaSound,hud):new Fruit(fruitsRegions.random(),fruitsSound,hud);
                    actors.addActor(actor);
                }
               hud.incrementFruitesPerRonda();
            }
        },2,5);

    }

//    private Actor getFruit() {
//        final Image fruitImage = new Image(fruitsRegions.random());
//
//        //Afegim les accions de pujar i baixar
//        getActions(fruitImage);
//
//        fruitImage.addListener(new InputListener(){
//            @Override
//            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                //parem objecte
//                fruitImage.clearActions();
//                //fem que no sigui clicable
//                fruitImage.setTouchable(null);
//                //animació d'eliminar objecte
//                fruitImage.addAction(Actions.parallel(
//                        Actions.fadeOut(1),
//                        Actions.scaleTo(2,2,1,Interpolation.pow2),
//                        Actions.sequence(Actions.delay(1),Actions.removeActor())
//                ));
//                //sound de bomba
//                fruitsSound.play();
//                return super.touchDown(event, x, y, pointer, button);
//            }
//        });
//
//        return fruitImage;
//    }

//    private void getActions(Image image) {
//        int pujada =MathUtils.random(700,900);
//        int desplasament = MathUtils.random(-100,100);
//        int rotacio = MathUtils.random(300,600);
//        int direccio = (MathUtils.random()<0.5f)?-1:1;
//        image.setOrigin(image.getWidth()/2,
//                image.getHeight()/2);
//
//        image.setPosition(MathUtils.random(0,1000),-300);
//
//        image.addAction(
//                Actions.sequence(
//                        Actions.delay(MathUtils.random()),
//                        Actions.moveBy(desplasament,pujada,2, Interpolation.pow2),
//                        Actions.moveBy(desplasament,-pujada,2,Interpolation.pow2)
//                ));
//        image.addAction(Actions.rotateBy(direccio*rotacio,4));
//    }

//    private Actor getBomba() {
//        final Image bombaImage = new Image(bomba);
//
//        //Afegim les accions de pujar i baixar
//        getActions(bombaImage);
//
//        bombaImage.addListener(new InputListener(){
//            @Override
//            public boolean touchDown(InputEvent event, float x, float y, int pointer, int button) {
//                //parem objecte
//                bombaImage.clearActions();
//                //fem que no sigui clicable
//                bombaImage.setTouchable(null);
//                //animació d'eliminar objecte
//                bombaImage.addAction(Actions.parallel(
//                        Actions.fadeOut(1),
//                        Actions.scaleTo(2,2,1,Interpolation.pow2),
//                        Actions.sequence(Actions.delay(1),Actions.removeActor())
//                ));
//                //sound de bomba
//                bombaSound.play();
//                //reiniciem contador
//                fruitsPerRonda=1;
//                return super.touchDown(event, x, y, pointer, button);
//            }
//        });
//
//        return bombaImage;
//    }



    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0.9f,0.9f,0.9f,1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height,true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}