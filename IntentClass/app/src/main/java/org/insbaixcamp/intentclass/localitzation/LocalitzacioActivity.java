package org.insbaixcamp.intentclass.localitzation;


import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.EditText;

import org.insbaixcamp.intentclass.MainActivity;
import org.insbaixcamp.intentclass.R;

public class LocalitzacioActivity extends MainActivity {
    //Buttons
    Button bMain;
    Button bLocalitzacio;
    //editText
    EditText etLocalitzacio;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_localitzacio);
        Log.i("Activity","LOCATION");
        //buttons
        bMain = findViewById(R.id.bMain);
        bMain.setOnClickListener(this);
        bLocalitzacio = findViewById(R.id.bLocalitzacio);
        bLocalitzacio.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        if (bMain.isPressed()) {
            // bWeb.setBackgroundColor(Color.RED);
            intent = new Intent(this, MainActivity.class);

        }else{
            etLocalitzacio= findViewById(R.id.etLocalitzacio);
            StringBuilder str = new StringBuilder();
            //afegin esquema de geolocatilitzacio https://maps.google.es/?q=Reus
            str.append("geo:0,0?q=");
            str.append(etLocalitzacio.getText());
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse(str.toString()));
        }
        startActivity(intent);
    }
}