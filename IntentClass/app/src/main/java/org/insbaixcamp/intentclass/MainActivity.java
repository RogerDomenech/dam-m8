package org.insbaixcamp.intentclass;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

import org.insbaixcamp.intentclass.communication.EmailActivity;
import org.insbaixcamp.intentclass.localitzation.LocalitzacioActivity;
import org.insbaixcamp.intentclass.navigation.WebActivity;
import org.insbaixcamp.intentclass.communication.TelefonActivity;

public class MainActivity extends AppCompatActivity implements OnClickListener{
    Button bWeb;
    Button bLocalitzacio;
    Button bTelefon;
    Button bEmail;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Log.i("Activity","Main");
        //Select els elements en view
        bWeb = findViewById(R.id.bWeb);
        bLocalitzacio =findViewById(R.id.bLocalitzacio);
        bTelefon= findViewById(R.id.bTelefon);
        bEmail=findViewById(R.id.bEmail);
        bWeb.setOnClickListener(this);
        bLocalitzacio.setOnClickListener(this);
        bTelefon.setOnClickListener(this);
        bEmail.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(bWeb.isPressed()){
            // bWeb.setBackgroundColor(Color.RED);
            Intent intentWeb= new Intent(this, WebActivity.class);
            startActivity(intentWeb);
        }
        if(bLocalitzacio.isPressed()){
            //bLocalitzacio.setBackgroundColor(Color.BLACK);
            Intent intentLocalitzacio= new Intent(this, LocalitzacioActivity.class);
            startActivity(intentLocalitzacio);
        }
        if(bTelefon.isPressed()){
            //bTelefon.setBackgroundColor(Color.BLUE);
            Intent intentTelefon= new Intent(this, TelefonActivity.class);
            startActivity(intentTelefon);
        }
        if(bEmail.isPressed()){
            ///bEmail.setBackgroundColor(Color.GREEN);
            Intent intentEmail= new Intent(this, EmailActivity.class);
            startActivity(intentEmail);
        }


    }
}
