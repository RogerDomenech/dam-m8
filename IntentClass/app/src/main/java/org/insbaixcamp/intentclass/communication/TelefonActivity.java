package org.insbaixcamp.intentclass.communication;


import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;

import org.insbaixcamp.intentclass.MainActivity;
import org.insbaixcamp.intentclass.R;

public class TelefonActivity extends MainActivity {
    //integer constant per acceir prermissos.
    private static final int PERMIS_TRUCADES=1;
    //boolena de controll permisos
    boolean control;
    //buttons
    Button bMain;
    ImageButton bTelefon;
    //EditText
    EditText etTelefon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_telefon);
        Log.i("Activity","PHONE");
        //buttons
        bMain = findViewById(R.id.bMain);
        bMain.setOnClickListener(this);
        bTelefon = findViewById(R.id.bTelefon);
        bTelefon.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        control=false;
        if (bMain.isPressed()) {
            // bWeb.setBackgroundColor(Color.RED);
            intent = new Intent(this, MainActivity.class);
            control=true;
        }else{
            etTelefon = findViewById(R.id.etTelefon);
            StringBuilder str = new StringBuilder();
            str.append("tel:");
            str.append(etTelefon.getText());
            intent = new Intent(Intent.ACTION_CALL, Uri.parse(str.toString()));
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED){
                control=true;
            }else{
                Toast.makeText(this,"Permisos no garantits", Toast.LENGTH_LONG).show();
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CALL_PHONE},PERMIS_TRUCADES);
                //Aqui sota no arribo mai, torno per "onRequestPErmisssionsResults
            }

        }
        if(control) {
            startActivity(intent);
        }else{
            Toast.makeText(this,"Error en ejecutacio", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        //super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMIS_TRUCADES:{
                    Log.i("lengGrandResults",String.valueOf(grantResults.length));
                    Log.i("PACKAGEMANAGER",String.valueOf(PackageManager.PERMISSION_GRANTED));
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    Toast.makeText(this, "Permisos garantits, gràcies", Toast.LENGTH_SHORT).show();
                    //retornem valor true per fer la starActivity;
                    control=true;
                }else{
                    Toast.makeText(this, "Permisos denegats :'(", Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}