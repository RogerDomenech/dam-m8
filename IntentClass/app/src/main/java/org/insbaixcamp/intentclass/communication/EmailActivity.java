package org.insbaixcamp.intentclass.communication;


import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import org.insbaixcamp.intentclass.MainActivity;
import org.insbaixcamp.intentclass.R;

import java.nio.file.Files;

public class EmailActivity extends MainActivity {
    //buttons
    Button bMain;
    Button bEmail;

    //editText
    EditText etPerA;
    EditText etAssumpte;
    EditText etCos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_email);
        Log.i("Activity","EMAIL");
        // decalracio d'elements
        //butons
        bMain = findViewById(R.id.bMain);
        bEmail = findViewById(R.id.bEmail);

        //events clickListener
        bMain.setOnClickListener(this);
        bEmail.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {
        Intent intent;
        if (bMain.isPressed()) {
            // bWeb.setBackgroundColor(Color.RED);
            intent = new Intent(this, MainActivity.class);

        }else{
            // es pulsat boto send
            //EditText
            etPerA = findViewById(R.id.etPerA);
            etAssumpte = findViewById(R.id.etAssumpte);
            etCos = findViewById(R.id.etCos);
            //String parts del correu
            String email = etPerA.getText().toString();
            String subject = etAssumpte.getText().toString();
            String body = etCos.getText().toString();

            intent=new Intent(Intent.ACTION_SEND);
            // construir la Uri per enviar un correu
            //Uri uri = Uri.parse("mailto:"+email).buildUpon().appendQueryParameter("subject",subject).appendQueryParameter("body",body).build();
            //intent = new Intent(Intent.ACTION_SENDTO,uri);
            intent.setType("text/plain");
            intent.putExtra(Intent.EXTRA_EMAIL, new String[]{email});
            intent.putExtra(Intent.EXTRA_SUBJECT,subject);
            intent.putExtra(Intent.EXTRA_TEXT,body);

        }
        startActivity(intent);
    }
}