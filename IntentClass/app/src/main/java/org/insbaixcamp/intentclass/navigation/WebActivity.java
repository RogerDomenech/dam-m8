package org.insbaixcamp.intentclass.navigation;


import android.app.SearchManager;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.os.Bundle;
import android.widget.EditText;

import org.insbaixcamp.intentclass.MainActivity;
import org.insbaixcamp.intentclass.R;

//heredo de la clase MainActivity
// preguntar al Oscar metode rapid per fer la importacio
// de una clase creada sense ser declara a ma en import

public class WebActivity extends MainActivity {
    Button bMain;
    Button bNavega;
    Button bCerca;
    //Edit text
    EditText etweb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web);
        Log.i("Activity","WEB");
        //buttons
        bMain = findViewById(R.id.bMain);
        bMain.setOnClickListener(this);
        bNavega = findViewById(R.id.bNavega);
        bNavega.setOnClickListener(this);
        bCerca = findViewById(R.id.bCerca);
        bCerca.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        if(bCerca.isPressed()){
            //editText
            etweb = findViewById(R.id.etWeb);
            intent = new Intent(Intent.ACTION_WEB_SEARCH);
            intent.putExtra(SearchManager.QUERY, etweb.getText().toString());
        }else if(bNavega.isPressed()){
            //editText
            etweb = findViewById(R.id.etWeb);
            //activity
            intent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+etweb.getText().toString()));

        }else{
            //if (bMain.isPressed()) {
            // bWeb.setBackgroundColor(Color.RED);
            intent = new Intent(this, MainActivity.class);
        }
        startActivity(intent);
    }
}
