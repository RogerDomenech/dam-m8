package org.insbaixcamp.firebase;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ValueEventListener, ChildEventListener {
//    private TextView tvCel;
//    private TextView tvHumitat;
//    private TextView tvTemperatura;
    private List<Prediccio> llistaPrediccio;
    private PrediccioAdapter prediccioAdapter;
    private RecyclerView rvTemperatures;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Write a message to the database
//        FirebaseDatabase database = FirebaseDatabase.getInstance();
//        DatabaseReference myRef = database.getReference("message");
//
//        myRef.setValue("Hello, World!");
//        tvCel = findViewById(R.id.tvCel);
//        tvHumitat = findViewById(R.id.tvHumitat);
//        tvTemperatura = findViewById(R.id.tvTemperatura);

//        DatabaseReference dbPrediccio =
//                FirebaseDatabase.getInstance().getReference()
//                        .child("prediccio_avui");
//                       // .child("cel");
//
//        dbPrediccio.addValueEventListener(new PrediccioListener());
//        dbPrediccio.addChildEventListener(new PrediccioListener());
        //BASE de Dades
        DatabaseReference dbPrediccio2 =
                FirebaseDatabase.getInstance().getReference()
                  .child("prediccions");

        llistaPrediccio = new ArrayList<Prediccio>();

        //Listeners
        dbPrediccio2.addValueEventListener(this);
        dbPrediccio2.addChildEventListener(this);


//        for (int i = 0; i<50; i++){
//            llistaPrediccio.add(new Prediccio("cel"+i,(double)i,20));
//        }
        prediccioAdapter = new PrediccioAdapter(llistaPrediccio);
//        Toast.makeText(MainActivity.this,"Size: "+prediccioAdapter.getItemCount(),Toast.LENGTH_SHORT).show();
        rvTemperatures = findViewById(R.id.rvTemperatures);
        // indica com montar els elements en el layout [LinearLayoutManager]
//        rvTemperatures.setLayoutManager(new GridLayoutManager(this,3));
        rvTemperatures.setLayoutManager(new LinearLayoutManager(this));
        rvTemperatures.addItemDecoration(new DividerItemDecoration(this,LinearLayoutManager.VERTICAL));
//        rvTemperatures.addItemDecoration(new DividerItemDecoration(this,GridLayoutManager.HORIZONTAL));
        //afegir /modificar entrada
        //Prediccio prediccio = new Prediccio("Enuvolat", 29, 32.3);
        //dbPrediccio2.child("20191108").setValue(prediccio);
        rvTemperatures.setAdapter(prediccioAdapter);
    }

        @Override
        public void onDataChange(DataSnapshot dataSnapshot) {
//            Prediccio prediccio = dataSnapshot.getValue(Prediccio.class);
//            tvCel.setText(prediccio.getCel());
//            tvHumitat.setText(prediccio.getHumitat()+"%");
//            tvTemperatura.setText(prediccio.getTemperatura()+"ºC");
            /*String cel = dataSnapshot.child("cel").getValue().toString();
            String humitat = dataSnapshot.child("humitat").getValue().toString();
            String temperatura = dataSnapshot.child("temperatura").getValue().toString();
            tvCel.setText(cel);
            tvHumitat.setText(humitat);
            tvTemperatura.setText(temperatura);*/

            // eliminem el contigut per evitar duplicats
            llistaPrediccio.removeAll(llistaPrediccio);
            //Recorrem els elements del dataSnapshot que conte el JSON de la DB
            for(DataSnapshot element : dataSnapshot.getChildren()){
                // creem un objecte classe Prediccio
                Prediccio prediccio = new Prediccio(
                        element.getKey(),
                        element.child("cel").getValue().toString(),
                        Double.valueOf(element.child("temperatura").getValue().toString()),
                        Double.valueOf(element.child("humitat").getValue().toString())
                );
                //afegim el element en la llistaPrediccio
                llistaPrediccio.add(prediccio);
            }
            prediccioAdapter.notifyDataSetChanged();
            //rvTemperatures.setAdapter(prediccioAdapter);
        }

        @Override
        public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//            Toast.makeText(MainActivity.this,"Has afegit "+
//                    dataSnapshot.getKey()+ ": " +
//                    dataSnapshot.getValue(),
//                    Toast.LENGTH_LONG).show();
        }

        @Override
        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {
//            Toast.makeText(MainActivity.this,"Has cambiat "+
//                            dataSnapshot.getKey()+ ": " +
//                            dataSnapshot.getValue(),
//                    Toast.LENGTH_LONG).show();
        }

        @Override
        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {
//            Toast.makeText(MainActivity.this,"Has eliminat "+
//                            dataSnapshot.getKey()+ ": " +
//                            dataSnapshot.getValue(),
//                    Toast.LENGTH_LONG).show();
        }

        @Override
        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

        }

        @Override
        public void onCancelled(DatabaseError databaseError) {
            Log.e("ERROR", "Error!", databaseError.toException());
        }
}
