package org.insbaixcamp.firebase;


import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class PrediccioAdapter extends RecyclerView.Adapter<PrediccioAdapter.ViewHolder> {

    public List<Prediccio> llistaPrediccio;

    public PrediccioAdapter(List<Prediccio> llistaPrediccio) {
        this.llistaPrediccio = llistaPrediccio;
    }
    // RecycleView
    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView tvCel;
        public TextView tvTempreratura;
        public TextView tvHumitat;
        public TextView tvData;

        // declara el contingut de la vista del RecyclerView
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCel = itemView.findViewById(R.id.tvCel);
            tvTempreratura = itemView.findViewById(R.id.tvTemperatura);
            tvHumitat = itemView.findViewById(R.id.tvHumitat);
            tvData=itemView.findViewById(R.id.tvData);
        }
    }

    @NonNull
    @Override
    public PrediccioAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // infla la vista amb el layout del pare
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_llista,parent,false);
        // retora una PrediccioAdapter.ViewHolder amb la view creada anteriorment.
        return new PrediccioAdapter.ViewHolder(view);
    }

    /// Montar cada element en la vista segons parametre
    @Override
    public void onBindViewHolder(@NonNull PrediccioAdapter.ViewHolder holder, int position) {
        Prediccio item = llistaPrediccio.get(position);
        holder.tvCel.setText(item.getCel());
        holder.tvHumitat.setText(String.valueOf(item.getHumitat()));
        holder.tvTempreratura.setText(String.valueOf(item.getTemperatura()));
        holder.tvData.setText(item.getData());
    }

    // Retorna el nombre de elements la llista
    @Override
    public int getItemCount() {
        return llistaPrediccio.size();
    }

}
