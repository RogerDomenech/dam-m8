package org.insbaixcamp.custombrowser;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.webkit.WebView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        WebView wvNavega = findViewById(R.id.wvNavega);

        //quan carrego l'app recullo la URL que hi ha dins de l'Intent
        if (getIntent().getData() != null) {
            wvNavega.loadUrl(getIntent().getData().toString());
        }

    }
}
