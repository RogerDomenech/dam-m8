package org.insbaixcamp.concatening;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.time.Clock;

public class MainActivity extends AppCompatActivity implements OnClickListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Button bConcatena =findViewById(R.id.bConcatena);
        Button bNoConcatena = findViewById(R.id.bNoConcatena);
        bConcatena.setOnClickListener(this);
        bNoConcatena.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if (v== findViewById(R.id.bConcatena)){
            // posem el boto que concatena (utilitzan String)
            concatena(v);
        }else{
            noConcatenis(v);
        }
    }
    private void concatena (View view){
        EditText etIteracions = findViewById(R.id.etIteracions);
        TextView tvConcatena = findViewById(R.id.tvConcatena);

        long tempsInicial;
        long tempsFinal;
        int iteracions = Integer.parseInt(etIteracions.getText().toString());
        // generem un String des de es.lipsum.com
        String str = new String();
        String strLorem = new String("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        tempsInicial= System.currentTimeMillis();
        for (int i = 0; i<iteracions; i++){
            str+=strLorem;
        }
        tempsFinal = System.currentTimeMillis();
        tvConcatena.setText(String.valueOf(tempsFinal-tempsInicial));

    }
    private void noConcatenis(View view){
        EditText etIteracions = findViewById(R.id.etIteracions);
        TextView tvConcatena = findViewById(R.id.tvNoConcatena);

        long tempsInicial;
        long tempsFinal;
        int iteracions = Integer.parseInt(etIteracions.getText().toString());
        // generem un String des de es.lipsum.com
        StringBuilder str = new StringBuilder();
        StringBuilder strLorem= new StringBuilder("Lorem ipsum dolor sit amet, consectetur adipiscing elit.");
        tempsInicial= System.currentTimeMillis();
        for (int i = 0; i<iteracions; i++){
            str.append(strLorem);
        }
        tempsFinal = System.currentTimeMillis();
        tvConcatena.setText(String.valueOf(tempsFinal-tempsInicial));

    }
}
