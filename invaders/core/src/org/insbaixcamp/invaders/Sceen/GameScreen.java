package org.insbaixcamp.invaders.Sceen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;

import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;

import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;

import org.insbaixcamp.invaders.Actor.Bullet;
import org.insbaixcamp.invaders.Actor.Enemic;
import org.insbaixcamp.invaders.Actor.Nau;
import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.HUD.HUD;
import org.insbaixcamp.invaders.InvadersGame;
import org.insbaixcamp.invaders.Actor.ParallaxBackground;

public class GameScreen implements Screen {
    public static Group bales;
    private final InvadersGame game;
    public static int score;
    private Stage stage;
    private Nau nau;
    private Group enemics;

    //private Group bales;
    private Timer.Task enemicTask;
    private HUD hud;

    public GameScreen(InvadersGame invadersGame) {

        this.game = invadersGame;
        enemics = new Group();
        bales = new Group();
        score=game.score;

    }

    @Override
    public void show() {
        //Escenari
        stage = new Stage(new FitViewport(Constants.X,Constants.Y));
        //Camptura la entrada
        Gdx.input.setInputProcessor(stage);
        //Actor
        nau = new Nau();
        hud = new HUD();
        //paralax
        stage.addActor(new ParallaxBackground());
        //add Actors to Stage
        stage.addActor(enemics);
        stage.addActor(bales);
        stage.addActor(nau);
        stage.addActor(hud);
        // creem enemic
        enemicTask=new Timer.Task() {
            @Override
            public void run() {
                for (int i = 0; i < 5 ; i++) {
                    enemics.addActor(new Enemic());
                }
            }
        };
        Timer.schedule(enemicTask,1,3);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
        //Colicions
        //bales
        for (Actor b :bales.getChildren()){
            Bullet bala = (Bullet)b;
            // la bala es d'un jugador
            if(bala.getVelocitat()>0) {
                for (Actor e : enemics.getChildren()) {
                    Enemic enemic = (Enemic) e;
                    if (bala.rect.overlaps(enemic.rect)) {
                        //elemina bala
                        bala.remove();
                        enemic.mor();
                        game.score++;
                    }
                }
            }else{
                //la bala es d'un enemics
                if(nau.rect.overlaps(bala.rect)){
                    bala.remove();
                    nau.mor();
                }
            }
        }

        //mirem si la nau es alive
        if(!nau.isAlive()){
            //nau.remove();
            game.setScreen(new GameOverScreen(game));
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height,true);

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
