package org.insbaixcamp.invaders.Sceen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.InvadersGame;

public class GameOverScreen implements Screen {
    private final InvadersGame game;
    private Skin skin;
    private Stage stage;
    private Table tableRoot;
    private Window windowRoot;
    private Label gameOverLabel;
    private Label scoreLabel;
    public int score;
    private TextButton buttonReturn;

    public GameOverScreen (InvadersGame game){
        this.game = game;
        score=game.score;
    }

    @Override
    public void show() {
        // carregem la skin
        skin = new Skin(Gdx.files.internal("neonui/neon-ui.json"));
        // stage
        stage = new Stage(new ExtendViewport(Constants.X/4, Constants.Y/4));
        // Actors
        tableRoot = new Table();
        windowRoot = new Window("Invaders", skin);

        //labels
        gameOverLabel = new Label("GAME OVER",skin);
        scoreLabel = new Label(""+score,skin);
        buttonReturn = new TextButton("Return Menu",skin);

        //montar table
        tableRoot.setFillParent(true);
        tableRoot.add(windowRoot);
        windowRoot.add(gameOverLabel).expandX().pad(5);
        windowRoot.row();
        windowRoot.add(scoreLabel).expandX().pad(5);
        windowRoot.row();
        windowRoot.add(buttonReturn).expandX().pad(5);

        //add stage
        stage.addActor(tableRoot);

        //listener
        Gdx.input.setInputProcessor(stage);
        buttonReturn.addListener(new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                game.setScreen(new MainMenuScreen(game));
            }
        });

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height,true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
