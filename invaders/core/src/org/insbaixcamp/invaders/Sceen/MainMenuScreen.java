package org.insbaixcamp.invaders.Sceen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.viewport.ExtendViewport;

import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.InvadersGame;

public class MainMenuScreen implements Screen {
    private final InvadersGame game;
    private Skin skin;
    private Stage stage;
    private Table tableRoot;
    private Window windowRoot;
    private TextButton buttonPlay;
    private TextButton buttonHScore;
    private TextButton buttonConfig;

    public MainMenuScreen (InvadersGame game){
        this.game = game;
    }
    @Override
    public void show() {
        // carregem la skin
        skin = new Skin(Gdx.files.internal("neonui/neon-ui.json"));
        // stage
        stage = new Stage(new ExtendViewport(Constants.X/4, Constants.Y/4));
        // Actors
        tableRoot = new Table();
        windowRoot = new Window("Invaders", skin);
        buttonPlay = new TextButton("Play",skin);
        buttonHScore = new TextButton("High Score",skin);
        buttonConfig = new TextButton("Config",skin);

        tableRoot.setFillParent(true);
        tableRoot.add(windowRoot.pad(10));
        windowRoot.add(buttonPlay).expandX().pad(2);
        windowRoot.row();
        windowRoot.add(buttonHScore).expandX().pad(2);
        windowRoot.row();
        windowRoot.add(buttonConfig).expandX().pad(2);
        windowRoot.row();
        stage.addActor(tableRoot);

        //inputs del stage
        Gdx.input.setInputProcessor(stage);
        //play
        buttonPlay.addListener( new ClickListener(){
            @Override
            public void clicked(InputEvent event, float x, float y) {
                super.clicked(event, x, y);
                game.setScreen(new GameScreen(game));
            }
        });

    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        stage.act(delta);
        stage.draw();
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width,height,true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}
