package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.ui.Image;

public class GameActor extends Image {
    public Rectangle rect;
    boolean alive;
    int lives;
    int maxLive;
    private Sound explosion;
    Color full = new Color(255, 255, 255, 255);
    Color middle = new Color(255, 125, 0, 255);
    Color low = new Color(255, 0, 0, 255);
    public GameActor(String texture) {
        super(new Texture(texture));
        explosion = Gdx.audio.newSound(Gdx.files.internal("explosion.wav"));
        alive= true;
        // centre de la nau
        this.setOrigin(this.getWidth()/2,this.getHeight()/2);
        rect = new Rectangle(getX(),getY(),getWidth(),getHeight());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        rect.setPosition(getX(),getY());
        changeColor();
    }
    public void changeColor(){
        if (alive){
            // si maxlive/live =1 => full
            // si live <= maxlive/2 =>middle
            // si live ==1 =>low
            if(lives>= maxLive/2) {
                // si el resultat de 100/live es menor a 100 full
                super.setColor(full);
            }else if(lives<= maxLive/2 && lives!=0) {
                // si el resultat de 100/live es menor a 50 middle
                super.setColor(middle);
            }else {
                // si el resultat de 100/live es menor a 33 low
                super.setColor(low);
            }
        }

    }

    public boolean isAlive(){
        return alive;
    }
    public void mor(){
        if(lives <= 0) {
            this.explosion.play();
            this.alive=false;
            remove();
        }else{
            lives -= 1;

        }

    }
}
