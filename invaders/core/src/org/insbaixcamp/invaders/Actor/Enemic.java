package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.Sceen.GameScreen;

public class Enemic extends GameActor {


    private int velocitat;
    //private Sound explosion;
    public Enemic() {
        super("invader2.png");
        rotateBy(180);
        //explosion = Gdx.audio.newSound(Gdx.files.internal("explosion.wav"));
        //position
        setPosition(MathUtils.random(0,Constants.X-getWidth()),Constants.Y);
        // vida enemic
        this.lives =3;
        maxLive=this.lives;
        velocitat=MathUtils.random(2,5);
        //disparar
        addAction(Actions.repeat(10,Actions.sequence(
                Actions.delay(MathUtils.random(1f,2f)),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        GameScreen.bales.addActor(new Bullet(getX()+getWidth()/2,getY()-100,-800));
                    }
                })
        )));
        //moviement
        addAction(
                Actions.sequence(
                        Actions.moveTo(getX(),-getHeight(),velocitat),
                        Actions.removeActor()
                )
        );
    }

    @Override
    public void mor() {
        super.mor();

    }

}
