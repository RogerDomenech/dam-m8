package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import org.insbaixcamp.invaders.Constants;

public class Bullet extends GameActor {

    private int velocitat;
    private final Sound shoot;


    public Bullet(float x, float y, int velocitat) {
        super("bullet.png");
        setPosition(x, y);
        this.velocitat = velocitat;
        if(velocitat<0){
            //girem la bala
            setRotation(180);

        }
        shoot = Gdx.audio.newSound(Gdx.files.internal("shoot.wav"));
        shoot.play();
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        moveBy(0,velocitat*delta);
        if(getY()> Constants.Y || getY()<0){
            remove();
        }
    }

    public int getVelocitat() {
        return velocitat;
    }

    public void setVelocitat(int velocitat) {
        this.velocitat = velocitat;
    }
}
