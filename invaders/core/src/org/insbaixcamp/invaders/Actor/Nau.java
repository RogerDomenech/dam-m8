package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.Sceen.GameScreen;

public class Nau extends GameActor {

    //private final Sound shoot;
    private final Sound shipexplosion;
    private int dir;
    private int velocitat=500;

    public Nau() {
        //textura
        super("ship2.png");
        this.lives=5;
        maxLive=this.lives;
        //position
        setPosition(Constants.X/2-getWidth()/2,-100);
        //animacio entrada
        addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.parallel(
                        Actions.alpha(1,0.5f),
                        Actions.moveBy(0,130,0.5f)
                )
        ));
        //Sound
        //shoot = Gdx.audio.newSound(Gdx.files.internal("shoot.wav"));
        shipexplosion = Gdx.audio.newSound((Gdx.files.internal("shipexplosion.wav")));
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        //us giroscopi
        dir=(int)-(Gdx.input.getAccelerometerX()/2);
        //escritory
        if(dir==0){
            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
                dir=1;
            }else if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
                dir=-1;
            }
        }
        //limits
        if(getX()<0){
            clearActions();
            setX(0);
        }else if(getX() > Constants.X-getWidth()) {
            clearActions();
            setX(Constants.X - getWidth());
        }else{
            moveBy(dir*velocitat*delta,0);
        }

        //disparem
        if(isAlive()&& Gdx.input.justTouched()){
            GameScreen.bales.addActor(
            new Bullet(getX()+getWidth()/2,getY()+getHeight(),500)
            );
        }
    }

    @Override
    public void mor() {
        this.lives--;
        if(this.lives < 0){
            alive=false;
        }
    }

    public Sound getShipexplosion() {
        return shipexplosion;
    }

    public int getDir() {
        return dir;
    }

    public void setDir(int dir) {
        this.dir = dir;
    }

    public int getVelocitat() {
        return velocitat;
    }

    public void setVelocitat(int velocitat) {
        this.velocitat = velocitat;
    }

    public int getLives() {
        return lives;
    }

    public void setLives(int lives) {
        this.lives = lives;
    }

}
