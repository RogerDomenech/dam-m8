package org.insbaixcamp.invaders;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import org.insbaixcamp.invaders.Sceen.GameScreen;
import org.insbaixcamp.invaders.Sceen.MainMenuScreen;

public class InvadersGame extends Game {
	SpriteBatch batch;
	Texture img;
	static public int score;
	
	@Override
	public void create () {
		this.score=0;
		setScreen(new MainMenuScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		//batch.dispose();
		//img.dispose();
	}
}
