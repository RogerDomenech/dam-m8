package org.insbaixcamp.invaders.HUD;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;

import org.insbaixcamp.invaders.Sceen.GameScreen;
import org.insbaixcamp.invaders.InvadersGame;

public class HUD extends Group {
    private final Skin skin;
    private final Label score;
    private int lives;
    int i;

    public HUD(){
        skin = new Skin(Gdx.files.internal("neonui/neon-ui.json"));
        score = new Label("",skin);
        score.setPosition(1000,1000,Align.right);
        score.setFontScale(5,5);
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        score.setText(GameScreen.score);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
    }
}
