package org.insbaixcamp.invaders.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.insbaixcamp.invaders.InvadersGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.height=1024;
		config.width=720;
		new LwjglApplication(new InvadersGame(), config);
	}
}
