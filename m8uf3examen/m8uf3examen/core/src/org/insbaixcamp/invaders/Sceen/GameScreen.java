package org.insbaixcamp.invaders.Sceen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.sun.org.apache.xpath.internal.operations.Bool;

import org.insbaixcamp.invaders.Actor.Boss;
import org.insbaixcamp.invaders.Actor.Bullet;
import org.insbaixcamp.invaders.Actor.Enemic;
import org.insbaixcamp.invaders.Actor.ExtraLive;
import org.insbaixcamp.invaders.Actor.GameActor;
import org.insbaixcamp.invaders.Actor.HUD;
import org.insbaixcamp.invaders.Actor.Nau;
import org.insbaixcamp.invaders.Actor.ParallaxBackground;
import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.InvadersGame;

public class GameScreen implements Screen {
    private final InvadersGame game;
    public static Stage stage;
    public static Nau nau;
    public static Group enemics;
    public static Group bales;
    public static Group powerUp;
    private Timer.Task enemicsTask;
    private Timer.Task bossTask;
    private static int score;
    private HUD hud;
    private ParallaxBackground parallaxBackground;



    public GameScreen(InvadersGame invadersGame) {
        this.game = invadersGame;
        enemics = new Group();
        bales = new Group();
        score = 0;
    }

    public static void addScore() {
        score++;
    }
    public static void addScore(int point){
        score+=point;
    }
    @Override
    public void show() {
        stage = new Stage(new FitViewport(Constants.X, Constants.Y));

        //Captura la entrada estandar
        Gdx.input.setInputProcessor(stage);

        //Creo la nau
        nau = new Nau();

        //Creo el HUD
        hud = new HUD();

        //Creo el fons del joc
        parallaxBackground = new ParallaxBackground();

        stage.addActor(parallaxBackground);
        stage.addActor(enemics);
        stage.addActor(bales);
        stage.addActor(nau);
        stage.addActor(hud);

        //Creem enemics
        enemicsTask = new Timer.Task() {
            @Override
            public void run() {
                for (int i = 0; i < MathUtils.random(2,3); i++) {
                    enemics.addActor(new Enemic());
                }

            }
        };
        Timer.schedule(enemicsTask, 1, 5);
        // Creem boss
        bossTask = new Timer.Task() {
            @Override
            public void run() {
                    enemics.addActor(new Boss());
            }
        };
        Timer.schedule(bossTask, 1, 30);
    }

    @Override
    public void render(float delta) {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        stage.act(delta);
        stage.draw();

        for (Actor b : bales.getChildren()) {
            Bullet bala = (Bullet) b;

            //bala disparada per la nau
            if (bala.getVelocitat() > 0) {
                for (Actor e : enemics.getChildren()) {
                        GameActor enemic = (GameActor)e;
                    if (bala.rect.overlaps(enemic.rect)) {
                        bala.remove();
                        enemic.mor();

                    }
                }
            }else{//bala disparada pels enemics
                if(bala.rect.overlaps(nau.rect)){
                    bala.remove();
                    nau.mor();
                }
            }
        }

        if(!nau.isAlive()){
            game.setScreen(new GameOverScreen(game,score));
        }
    }

    @Override
    public void resize(int width, int height) {
        stage.getViewport().update(width, height, true);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        stage.dispose();
    }
}
