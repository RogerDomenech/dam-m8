package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;

import com.badlogic.gdx.scenes.scene2d.Actor;
import org.insbaixcamp.invaders.Sceen.GameScreen;

public class ExtraLive extends GameActor {
    private Sound explosion;
    private float x;
    private float y;
    public ExtraLive(float x, float y) {
        super("extralive.png");
        this.x=x;
        this.y=y;
        explosion = Gdx.audio.newSound(Gdx.files.internal("explosion.wav"));
        setPosition(x,y);
        vides = 1;
    }

    @Override
    public void mor() {
        vides--;
        if(vides < 0){
            Nau.addlive(1);
            explosion.play();
            super.mor();
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        for (Actor b : GameScreen.bales.getChildren()) {
            Bullet bala = (Bullet) b;

            //bala disparada per la nau
            if (bala.getVelocitat() > 0) {

                if (bala.rect.overlaps(this.rect)) {
                    bala.remove();
                    this.mor();

                }
            }
        }
    }
}
