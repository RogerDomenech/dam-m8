package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.utils.Timer;

import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.Sceen.GameScreen;

public class Nau extends GameActor {
    private final Sound shipexplosion;
    private Sound shoot;
    private int dir;
    private int velocitat;
    private Timer.Task shotTask;
    public static int vides;

    public Nau() {
        super("ship3.png");
        velocitat = 500;
        vides = 3;

        setPosition(Constants.X /2 - getWidth()/2,-100);
        shoot = Gdx.audio.newSound(Gdx.files.internal("shoot.wav"));
        shipexplosion = Gdx.audio.newSound(Gdx.files.internal("shipexplosion.wav"));

        //Animació entrada
        addAction(Actions.sequence(
                Actions.alpha(0),
                Actions.parallel(
                        Actions.alpha(1,0.5f),
                        Actions.moveBy(0,130,0.5f)
                )
        ));

        //Disparem en automatic
        shotTask = new Timer.Task() {
            @Override
            public void run() {
                if(isAlive()){
                    GameScreen.bales.addActor(
                            new Bullet(getX() + getWidth()/2 , getY()+ getHeight(),500)
                    );
                    //shoot.play();
                }
            }
        };
        Timer.schedule(shotTask, 1, 0.2f);
    }

    @Override
    public void act(float delta) {
        super.act(delta);


        //giroscopi
        dir = (int) -(Gdx.input.getAccelerometerX()/2);

        //escriptori
        if(dir == 0){
            if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
                dir =1;
            } else if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
                dir = -1;
            }
        }

        if(getX() < 0){//limit esquerra
            clearActions();
            setX(0);
        }else if(getX() > Constants.X - getWidth()){//limit dreta
            clearActions();
            setX(Constants.X - getWidth());
        }else {//moviment
            moveBy(dir * velocitat * delta, 0);
        }

    }

    @Override
    public void mor() {
        vides--;
        if(vides < 0){
            alive = false;
            addAction(Actions.sequence(
                    Actions.delay(1),
                    Actions.removeActor()
            ));
        }
    }
    static void addlive(int live){
        vides+=live;
    }

}
