package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.Sceen.GameScreen;

public class Boss extends GameActor {
    private Sound explosion;

    public Boss() {
        super("boss.png");
        explosion = Gdx.audio.newSound(Gdx.files.internal("explosion.wav"));

        setPosition(super.getWidth(),Constants.Y-super.getHeight());

        vides = 10;

        //L'enemic es mou a la part inferior de la pantalla i desapareig
        addAction(
                Actions.sequence(
                        Actions.moveTo(Constants.X-super.getWidth(),Constants.Y-super.getHeight(),MathUtils.random(10,30)),
                        Actions.removeActor()
                )
        );
    }

    @Override
    public void mor() {
        vides--;
        if(vides < 0){
            GameScreen.addScore(10);
            explosion.play();
            super.mor();
            GameScreen.stage.addActor(new ExtraLive(getX(),getY()));
        }
    }

}
