package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;
import com.badlogic.gdx.scenes.scene2d.actions.DelayAction;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.utils.Timer;

import org.insbaixcamp.invaders.Constants;


public class GameActor extends Image {
    public int vides;
    public Rectangle rect;
    public boolean alive;
    public GameActor(String textura){
        super(new Texture(textura));
        alive = true;

        //l'origien de la imatge es el centre de la imatge
        this.setOrigin(this.getWidth()/2, this.getHeight()/2);
        rect = new Rectangle(getX(),getY(),getWidth(),getHeight());
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        rect.setPosition(getX(),getY());
        Timer.schedule(new Timer.Task() {
            @Override
            public void run() {
                garbage();
            }
        },5);
//        if (delta>0.3f){
//            garbage();
//        }

    }

    public boolean isAlive() {
        return alive;
    }

    public void mor() {
        this.alive = false;
        remove();

    }

    public void garbage(){
        if(getX()>Constants.X||getY() > Constants.Y || getX()<0 || getY()<0){
            mor();
        }
    }

}
