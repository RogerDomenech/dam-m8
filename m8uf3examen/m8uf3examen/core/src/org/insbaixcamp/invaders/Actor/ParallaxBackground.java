package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.utils.Array;

import org.insbaixcamp.invaders.Constants;

public class ParallaxBackground extends Actor {
    private int scroll;
    private Array<Texture> layers;
    private final int LAYER_SPEED_DIFFERENCE = 2;

    float x,y,width,heigth,scaleX,scaleY;
    int originX, originY,rotation,srcX,srcY;
    boolean flipX,flipY;

    private int speed;

    public ParallaxBackground(){

        layers = new Array<Texture>();
        for(int i = 0; i < 2;i++){
            layers.add(new Texture(Gdx.files.internal("parallax/img"+i+".png")));
            layers.get(i).setWrap(Texture.TextureWrap.Repeat, Texture.TextureWrap.Repeat);
        }
        scroll = 0;
        speed = 1;

        x = y = originX = originY = rotation = srcY = 0;
        width = Constants.X;
        heigth = Constants.Y;
        scaleX = scaleY = 1;
        flipX = flipY = false;
    }

    public void setSpeed(int newSpeed){
        this.speed = newSpeed;
    }


    @Override
    public void draw(Batch batch, float parentAlpha) {
        batch.setColor(getColor().r, getColor().g, getColor().b, getColor().a * parentAlpha);

        scroll+=speed;
        for(int i = 0;i<layers.size;i++) {
            srcX = scroll + i*this.LAYER_SPEED_DIFFERENCE *scroll;
            batch.draw(layers.get(i), x, y, originX, originY, width, heigth,scaleX,scaleY,rotation,srcY,-srcX,layers.get(i).getWidth(),layers.get(i).getHeight(),flipX,flipY);
        }
    }
}