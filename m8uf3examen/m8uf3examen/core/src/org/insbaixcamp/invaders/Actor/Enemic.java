package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.scenes.scene2d.actions.Actions;

import org.insbaixcamp.invaders.Constants;
import org.insbaixcamp.invaders.Sceen.GameScreen;

public class Enemic extends GameActor {
    private Sound explosion;

    public Enemic() {
        super("invader3.png");
        explosion = Gdx.audio.newSound(Gdx.files.internal("explosion.wav"));
        //girem la imatge
        //rotateBy(180);

        setPosition(
                MathUtils.random(0, Constants.X-getWidth()),
                Constants.Y+ MathUtils.random(10,300));
        vides = 3;
        //L'enemic dispara cada segon
        addAction(Actions.repeat(10,Actions.sequence(
                Actions.delay(MathUtils.random(2f,5f)),
                Actions.run(new Runnable() {
                    @Override
                    public void run() {
                        GameScreen.bales.addActor(
                                new Bullet(getX()+getWidth()/2,getY(),-800)
                        );
                    }
                })
        )));

        //L'enemic es mou a la part inferior de la pantalla i desapareig
        addAction(
                Actions.sequence(
                        Actions.moveTo(getX(),-getWidth(),MathUtils.random(7,9)),
                        Actions.removeActor()
                )
        );
    }

    @Override
    public void mor() {
        vides--;
        if(vides < 0){
            GameScreen.addScore();
            explosion.play();
            super.mor();
        }
    }


}
