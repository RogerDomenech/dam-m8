package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.graphics.Color;

import org.insbaixcamp.invaders.Constants;

public class Bullet extends GameActor {
    private int velocitat;

    public void setVelocitat(int velocitat) {
        this.velocitat = velocitat;
    }

    public int getVelocitat() {
        return velocitat;
    }

    public Bullet(float x, float y , int velocitat) {
        super("bullet2.png");
        setPosition(x,y);
        this.velocitat = velocitat;
        if(velocitat<0){// Si la bala es del enemic la girem i canviem de color
            setRotation(180);
            setColor(Color.CORAL);
        }
    }

    @Override
    public void act(float delta) {
        super.act(delta);
        moveBy(0, velocitat * delta);
    }
}
