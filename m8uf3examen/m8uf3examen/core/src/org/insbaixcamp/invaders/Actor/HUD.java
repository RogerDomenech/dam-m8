package org.insbaixcamp.invaders.Actor;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;

import org.insbaixcamp.invaders.Sceen.GameScreen;

import javax.swing.GroupLayout;

public class HUD extends Group {

    private final Skin skin;
    private final Label videsLabel, objectesLabel;
    private int vides;
    int i;
    public HUD(){
    skin = new Skin(Gdx.files.internal("neonui/neon-ui.json"));
    videsLabel = new Label("",skin);
    videsLabel.setPosition(50,1000);

    objectesLabel = new Label("",skin);
    objectesLabel.setPosition(100,1000);


        //Fem mes gran el text perquè es vegui be
    videsLabel.setFontScale(5,5);
    objectesLabel.setFontScale(5,5);

}


    @Override
    public void act(float delta) {
        super.act(delta);
        videsLabel.setText(GameScreen.nau.vides);

//        TODO: Descomentar per veure el nombre d'objectes de l'escenari
        i=0;
        for (Actor actor : GameScreen.bales.getChildren()
             ) {
            i++;
        }
        for (Actor actor : GameScreen.enemics.getChildren()
        ) {
            i++;
        }
        objectesLabel.setText(i);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        videsLabel.draw(batch,parentAlpha);

//      TODO: Descomentar per veure el nombre d'objectes de l'escenari
        objectesLabel.draw(batch,parentAlpha);
    }
}
