package org.insbaixcamp.invaders;


import com.badlogic.gdx.Game;


import org.insbaixcamp.invaders.Sceen.GameOverScreen;
import org.insbaixcamp.invaders.Sceen.GameScreen;
import org.insbaixcamp.invaders.Sceen.MainMenuScreen;

public class InvadersGame extends Game {

	@Override
	public void create () {

		setScreen(new MainMenuScreen(this));
	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
	}
}
