package org.provafinaluf1.mapsactivity;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Point;
import android.os.Bundle;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener {
    private static final int PERMIS_LOCALITZACIO_PRECISA = 1;

    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }


    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        //LatLng sydney = new LatLng(-34, 151);
        //LatLng reus = new LatLng(41.1, 1.1);
        LatLng reus = new LatLng(0, 0);
        //mMap.addMarker(new MarkerOptions().position(reus).title("Marker in Reus"));
        afegirMarcador(reus,"Marker a Reus");
        //CameraPosition cameraPosition = new CameraPosition(reus,17,0,0);
        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(reus) // centrem el mapa
                .zoom(1)  // zoom
                .bearing(0)  // Orientacio North
                .tilt(90)   //punt de vista
                .build();
        //mMap.moveCamera(CameraUpdateFactory.newLatLng(reus));
        //mMap.setMapType(GoogleMap.MAP_TYPE_SATELLITE);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        //mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        //carga de acamara amb animació
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        // listener OnMapClick
        mMap.setOnMapClickListener(this);
        habilitaLocalització();
    }

    private void afegirMarcador(LatLng latitudLongitiud, String titol){
        mMap.addMarker(new MarkerOptions().position(latitudLongitiud).title(titol));
    }


    @Override
    public void onMapClick(LatLng latLng) {
        Projection projection  = mMap.getProjection();
        Point coord = projection.toScreenLocation(latLng);

        Toast.makeText(MapsActivity.this, "Click\n"+
                "\nLat: "+latLng.latitude+
                "\nLng: "+latLng.longitude +
                "\nX: "+coord.x+
                "\nY: "+coord.y
                ,Toast.LENGTH_SHORT).show();
    }
    //Mètode implementat de la inferficie GoogleMap.OnMapLongClickListener
    @Override
    public void onMapLongClick(LatLng latLng) {
        Projection projection = mMap.getProjection();
        Point coord = projection.toScreenLocation(latLng);

        Toast.makeText(MapsActivity.this,
                "Long click" +
                        "\nLat: " + latLng.latitude+
                        "\nLng: " + latLng.longitude+
                        "\nX: " + coord.x+
                        "\nY: " + coord.y,
                Toast.LENGTH_SHORT).show();
    }

    private void habilitaLocalització(){
        if(ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED){
          mMap.setMyLocationEnabled(true);
        }else {
            //Demanar permis usuari per la geolocalització
            ActivityCompat.requestPermissions(
                    this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, PERMIS_LOCALITZACIO_PRECISA);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case PERMIS_LOCALITZACIO_PRECISA:{
                if(grantResults.length>0 && grantResults[0]==PackageManager.PERMISSION_GRANTED){
                    habilitaLocalització();
                }else {
                    // si no tennim permis, podrim fer una altre cosa
                }
            }
        }
    }
}
