package org.provafinaluf2.rogerdomenech.ui.map;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import org.provafinaluf2.rogerdomenech.R;

public class MapsFragment extends Fragment implements OnMapReadyCallback {

    private GoogleMap mMap;
    private View rootView;
    SupportMapFragment mapFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //Prepearem la vista
        rootView= inflater.inflate(R.layout.fragment_maps, container, false);
        // montem el Fragment que contindra el mapa
        mapFragment= (SupportMapFragment) getChildFragmentManager().findFragmentById(R.id.map);
        // Actualitzem el contigut del mapa
        mapFragment.getMapAsync(this);
        // retornem la vista
        return rootView;
    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        //Creacio del marcador amb les coordenades
        LatLng shibuya = new LatLng(35.6595062, 139.7005684);
        //Afegim el marcador al mapa amb la posicio i el titol donat per string
        mMap.addMarker(new MarkerOptions().position(shibuya).title(getString(R.string.mk_shibuya)));
        //Centrem la camera al punt on esta el marcador shibuya
        mMap.moveCamera(CameraUpdateFactory.newLatLng(shibuya));
        //Realitzem un zoom de 20 en el marcador
        mMap.moveCamera(CameraUpdateFactory.zoomTo(20));
        //Assignem per defecte un tipus Hibrid en el mapa
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }
}
