package org.provafinaluf2.rogerdomenech.ui.quisoc;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import org.provafinaluf2.rogerdomenech.R;

public class QuisocFragment extends Fragment {


    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //preparem a vista
        View root = inflater.inflate(R.layout.fragment_quisoc, container, false);

        //retornem la vista
        return root;
    }
}