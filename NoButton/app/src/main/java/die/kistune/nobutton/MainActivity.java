package die.kistune.nobutton;

import android.media.MediaPlayer;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import java.util.Random;

public class MainActivity extends AppCompatActivity {
    ImageView iv_redButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //imageView
        iv_redButton = findViewById(R.id.iv_redButton);
        iv_redButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Integer[] listSound = {   R.raw.sound1,
                                                R.raw.sound2,
                                                R.raw.sound3,
                                                R.raw.sound4,
                                                R.raw.sound5,
                                                R.raw.sound6,
                                                R.raw.sound7,
                                                R.raw.sound8,
                                                R.raw.sound9,
                                                R.raw.sound10};
                Random rand = new Random();
                final int music = rand.nextInt((listSound.length - 1) + 1) + 0;
                final MediaPlayer mediaPlayer = MediaPlayer.create(MainActivity.this, listSound[music]);
                    mediaPlayer.start();
                    mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
                    public void onCompletion(MediaPlayer mp) {
                        mp.release();
                    };
                });
            }
        });

    }

}
