package org.insbaixcamp.test;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.constraintlayout.widget.Constraints;

import android.graphics.Color;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.text.Layout;
import android.widget.Toast;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.insbaixcamp.firebase.Prediccio;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements ValueEventListener, ChildEventListener {
    private List<Prediccio> llistaPrediccio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ConstraintLayout background = findViewById(R.id.test);
        background.setBackgroundColor(Color.blue(500));
        llistaPrediccio = new ArrayList<Prediccio>();
        DatabaseReference dbPrediccio2 =
                FirebaseDatabase.getInstance().getReference()
                        .child("prediccions");
        dbPrediccio2.addValueEventListener(this);
        dbPrediccio2.addChildEventListener(this);


    }

    @Override
    public void onChildAdded(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

    }

    @Override
    public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

    }

    @Override
    public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
        String json="";
        for(DataSnapshot element : dataSnapshot.getChildren()){
            // creem un objecte classe Prediccio
            Prediccio prediccio = new Prediccio(
                    element.getKey(),
                    element.child("cel").getValue().toString(),
                    Double.valueOf(element.child("temperatura").getValue().toString()),
                    Double.valueOf(element.child("humitat").getValue().toString())
            );
            //afegim el element en la llistaPrediccio
            llistaPrediccio.add(prediccio);
        }
        for(org.insbaixcamp.firebase.Prediccio prediccio : llistaPrediccio){
                json+=prediccio.toString();
        }
        Toast.makeText(MainActivity.this,"Size: "+json,Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onCancelled(@NonNull DatabaseError databaseError) {

    }
}
