package org.insbaixcamp.k2mconverter;

import android.text.Editable;
import android.text.TextWatcher;
import android.widget.TextView;

public class CostumTextWatcher implements android.text.TextWatcher {
    private TextView tv1,tv2;

    public CostumTextWatcher(TextView tv1, TextView tv2){
        this.tv1 = tv1;
        this.tv2 = tv2;
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        //tv1.setText(R.string.tv_anterior+s.toString());
        String str = getString(R.string.K2M);
        tv1.setText(R.string.tv_anterior+s.toString());
    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        tv2.setText(R.string.tv_actual+s.toString());
    }

    @Override
    public void afterTextChanged(Editable s) {
    // no usable
    }
}
