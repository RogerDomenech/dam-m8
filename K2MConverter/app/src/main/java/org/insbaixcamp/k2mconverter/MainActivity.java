package org.insbaixcamp.k2mconverter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import java.text.DecimalFormat;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //entrades
    EditText etDistancia;
    //sortides
    TextView tvDistancia;
    TextView tvAnterior;
    TextView tvActual;
    //groupRadioButons
    RadioGroup rgBotons;
    //RadioButtons
    RadioButton rbKilometres;
    RadioButton rbMilles;
    //boto de calcular
    Button bCalcula;
    //decimal format
    DecimalFormat decimal= new DecimalFormat("#.##");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Entrada
        etDistancia= findViewById(R.id.etDistancia);
        //addTextChangerListener es un listener sobre el text
        // la CostumTextWatcher es una class custom  de TextWatcher
        etDistancia.addTextChangedListener(new CostumTextWatcher(tvAnterior,tvActual));
        //Sortides
        tvDistancia = findViewById(R.id.tvDistancia);
        tvActual = findViewById(R.id.tvActual);
        tvAnterior = findViewById(R.id.tvAnterior);
        //radioButtons
        rbKilometres = findViewById(R.id.rbKilometres);
        rbMilles = findViewById(R.id.rbMilles);
        rgBotons = findViewById(R.id.rgBotons);
        rgBotons.getCheckedRadioButtonId();
        //Buttons
        bCalcula = findViewById(R.id.bCalcula);
        bCalcula.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        if(bCalcula.isPressed()&&checkNumber(etDistancia)){
            if(rbKilometres.isChecked()){
                tvDistancia.setText(getString(R.string.K2M,String.valueOf(calculaKmMilles())));
            }else if (rbMilles.isChecked()){
                tvDistancia.setText(getString(R.string.M2K,String.valueOf(calculaMillesKm())));
            }else{
                tvDistancia.setText(R.string.tv_error);
            }

        }
    }

    public String calculaKmMilles(){
        float valor;
        // suposem que convertim de km a milles
        valor = Float.parseFloat(etDistancia.getText().toString());
        return (decimal.format(valor*1.609344f));
    }
    public String calculaMillesKm(){
        float valor;
        // suposem que convertim de milles a Km
        valor = Float.parseFloat(etDistancia.getText().toString());
        return(decimal.format(valor*0.621371192f));
    }
    public boolean checkNumber(EditText etDistancia){
        String distancia= etDistancia.getText().toString();
        if(!distancia.isEmpty()||!distancia.equals("")){
            return true;
        }else{
            return false;
        }
    }

}
