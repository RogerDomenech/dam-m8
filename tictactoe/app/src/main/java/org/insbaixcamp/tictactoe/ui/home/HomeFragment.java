package org.insbaixcamp.tictactoe.ui.home;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.navigation.Navigation;

import org.insbaixcamp.tictactoe.R;
import org.insbaixcamp.tictactoe.utils.Constant;
import org.w3c.dom.Text;

public class HomeFragment extends Fragment {
    private static final String TAG = "HomeFragment";

    private View root;
    private TextView tvId;
    private Button bSing;
    private Button bPlay;
    private Button bSetting;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        root = inflater.inflate(R.layout.fragment_home, container, false);
        tvId = root.findViewById(R.id.tvUserID);
        bSing = root.findViewById(R.id.bSingIn);
        bPlay = root.findViewById(R.id.bPlay);
        bSetting = root.findViewById(R.id.bSettings);
        // perque funcioni els indicadors de nav_game i nav_setting
        bPlay.setOnClickListener(Navigation.createNavigateOnClickListener(R.id.nav_game));
        bSetting.setOnClickListener((Navigation.createNavigateOnClickListener(R.id.nav_setting)));
        readUserInfo();
        return root;
    }
    private void readUserInfo(){
        SharedPreferences prefs = getActivity().getSharedPreferences(getActivity().getPackageName()+"_preferences", Context.MODE_PRIVATE);
        if(prefs.getString(Constant.USER_ID, null)!=null){
            tvId.setText("Id: "+prefs.getString(Constant.USER_ID,null));

            bSing.setEnabled(false);
            bPlay.setEnabled(true);
        }else{
            bSing.setEnabled(false);
            bPlay.setEnabled(true);
        }
    }
}