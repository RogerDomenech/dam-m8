package org.insbaixcamp.tictactoe.models;

public class User {
    private String id;

    public User(String id) {
        this.id = id;
    }
    public User(){

    }
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                '}';
    }
}
