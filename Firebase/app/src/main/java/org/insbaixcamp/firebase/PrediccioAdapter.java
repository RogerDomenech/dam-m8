package org.insbaixcamp.firebase;


import android.content.Context;
import android.text.Layout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.PopupMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.database.DatabaseReference;

import java.util.List;

public class PrediccioAdapter extends RecyclerView.Adapter<PrediccioAdapter.ViewHolder> {

    public List<Prediccio> llistaPrediccio;
    private Context context;
    private DatabaseReference databaseReference;

    public PrediccioAdapter(Context context,
                            DatabaseReference databaseReference,
                            List<Prediccio> llistaPrediccio) {
        this.llistaPrediccio = llistaPrediccio;
        this.context = context;
        this.databaseReference = databaseReference;
    }
    // RecycleView
    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{
        public TextView tvCel;
        public TextView tvTempreratura;
        public TextView tvHumitat;
        public TextView tvData;

        // declara el contingut de la vista del RecyclerView
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            tvCel = itemView.findViewById(R.id.tvCel);
            tvTempreratura = itemView.findViewById(R.id.tvTemperatura);
            tvHumitat = itemView.findViewById(R.id.tvHumitat);
            tvData=itemView.findViewById(R.id.tvData);
            //posem el listener en cada element passat per parametre
            itemView.setOnClickListener(this);
        }
        //metode a implemantar la interficiea View.onclickListener
        @Override
        public void onClick(View v) {
            int positcio = getAdapterPosition();
            //Toast.makeText(context,"Has Click in: "+positcio,Toast.LENGTH_LONG).show();
            Log.i("test",String.valueOf(positcio));
            mostraPopapMenu(v,positcio);
        }
    }

    @NonNull
    @Override
    public PrediccioAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // infla la vista amb el layout del pare
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_llista,parent,false);
        // retora una PrediccioAdapter.ViewHolder amb la view creada anteriorment.
        return new PrediccioAdapter.ViewHolder(view);
    }

    /// Montar cada element en la vista segons parametre
    // enllaç dades amb vista
    @Override
    public void onBindViewHolder(@NonNull PrediccioAdapter.ViewHolder holder, int position) {
        Prediccio item = llistaPrediccio.get(position);
        holder.tvCel.setText(item.getCel());
        holder.tvHumitat.setText(String.valueOf(item.getHumitat())+" %");
        holder.tvTempreratura.setText(String.valueOf(item.getTemperatura())+" ºC");
        // convertim en format  dd/mm/yyyy
        String data = Formatdate(item.getData());
        holder.tvData.setText(data);
    }

    /**
     * funcio que retorna una data en String en format YYYYMMDD -> DD/MM/YYYY
     * @return  String Data en format DD/MM/YYYY
     * */
    private String Formatdate(String data){
        // iniciem StringBuilder
        StringBuilder builder = new StringBuilder(data);
        // invertim data
        String dataInverted = builder.reverse().toString();
        StringBuilder dia = new StringBuilder(dataInverted.substring(0,2));
        StringBuilder mes =  new StringBuilder(dataInverted.substring(2,4));
        StringBuilder any =  new StringBuilder(dataInverted.substring(4));
        // retornem el valor montat, cada element es te que revertir ja que ho retorna invers
        return dia.reverse().toString()+"/"+mes.reverse().toString()+"/"+any.reverse().toString();

    }
    // Retorna el nombre de elements la llista
    @Override
    public int getItemCount() {
        return llistaPrediccio.size();
    }

    private void mostraPopapMenu(View view, int position){
        PopupMenu popupMenu = new PopupMenu(this.context,view);
        MenuInflater menuInflater = popupMenu.getMenuInflater();
        menuInflater.inflate(R.menu.menu_main, popupMenu.getMenu());
        popupMenu.setOnMenuItemClickListener(new Menu(position));
        popupMenu.show();
    }

    public class Menu implements PopupMenu.OnMenuItemClickListener{
        int posicio;

        public Menu (int posicio){
            this.posicio = posicio;
        }

        @Override
        public boolean onMenuItemClick(MenuItem item) {
            // booean de return
            boolean menuClicat =false;
            Prediccio prediccio;
            String cel = llistaPrediccio.get(posicio).getCel();
            Double temperatura = llistaPrediccio.get(posicio).getTemperatura();
            Double humitat= llistaPrediccio.get(posicio).getHumitat();
            String data = llistaPrediccio.get(posicio).getData();
            switch (item.getItemId()){
                case R.id.mIncrementa:
                    //incrementem Entrada
                   /* prediccio= new Prediccio(llistaPrediccio.get(posicio).getCel(),
                            llistaPrediccio.get(posicio).getTemperatura()+1,
                            llistaPrediccio.get(posicio).getHumitat());*/
                    //databaseReference.child(llistaPrediccio.get(posicio).getData()).setValue(prediccio);
                    temperatura++;
                    menuClicat=true;
                    break;
                case R.id.mDecrementa:
                    //decrementem entrada
                   /* prediccio= new Prediccio(llistaPrediccio.get(posicio).getCel(),
                            llistaPrediccio.get(posicio).getTemperatura()-1,
                            llistaPrediccio.get(posicio).getHumitat());*/
                    // databaseReference.child(llistaPrediccio.get(posicio).getData()).setValue(prediccio);
                    temperatura--;
                    menuClicat=true;
                    break;
                case R.id.hIncrementa:
                    humitat++;
                    menuClicat=true;
                    break;

                case R.id.hDecrementa:
                    humitat--;
                    menuClicat=true;
                    break;

                default:
                    menuClicat = false;
            }
            prediccio=new Prediccio(cel,temperatura,humitat);
            databaseReference.child(data).setValue(prediccio);
            return menuClicat;
        }
    }
}
