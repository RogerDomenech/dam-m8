package org.insbaixcamp.firebase;

public class Prediccio {
    private String cel;
    private double temperatura;
    private double humitat;
    private String data;

    public Prediccio() {
        //Es obligatorio incluir constructor por defecto
    }

    public Prediccio( String cel, double temperatura, double humitat) {
        this.cel = cel;
        this.temperatura = temperatura;
        this.humitat = humitat;
    }

    public Prediccio(String data, String cel, double temperatura, double humitat) {
        this.cel = cel;
        this.temperatura = temperatura;
        this.humitat = humitat;
        this.data = data;
    }

    public String getCel() {
        return cel;
    }

    public void setCel(String cel) {
        this.cel = cel;
    }

    public double getTemperatura() {
        return temperatura;
    }

    public void setTemperatura(double temperatura) {
        this.temperatura = temperatura;
    }

    public double getHumitat() {
        return humitat;
    }

    public void setHumitat(double humitat) {
        this.humitat = humitat;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
