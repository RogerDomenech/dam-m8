package org.insbaixcamp.lifecycle;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        TextView tvOnCreateCounter = findViewById(R.id.tvOnCreateCounter);
        //agafem  el valor del tvOnCreateConunter i l'increment
        //0tvOnCreateCounter.setText(String.valueOf(Integer.parseInt(tvOnCreateCounter.getText().toString() +1)));
        tvOnCreateCounter.setText("1");
        Toast.makeText(this,"onCreate()",Toast.LENGTH_SHORT).show();

    }
    @Override
    protected void onStart(){
        super.onStart();
        TextView tvOnStartCounter = findViewById(R.id.tvOnStartCounter);
        //agafem el valor del OnStarCounter i el incrementem
        tvOnStartCounter.setText(String.valueOf(Integer.parseInt(tvOnStartCounter.getText().toString())+1));
    }
    @Override
    protected void onStop(){
        super.onStop();
        TextView tvOnStopCounter = findViewById(R.id.tvOnStopCounter);
        tvOnStopCounter.setText(String.valueOf(Integer.parseInt(tvOnStopCounter.getText().toString())+1));
    }
    @Override
    protected void onResume(){
        super.onResume();
        TextView tvOnResumeCounter = findViewById(R.id.tvOnResumeCounter);
        tvOnResumeCounter.setText(String.valueOf(Integer.parseInt(tvOnResumeCounter.getText().toString())+1));
    }
    @Override
    protected void onDestroy(){
        super.onDestroy();
        TextView tvOnDestroyCounter = findViewById(R.id.tvOnDestroy);
       // tvOnDestroyCounter.setText(String.valueOf(Integer.parseInt(tvOnDestroyCounter.getText().toString())+1));
        tvOnDestroyCounter.setText("1");
        Toast.makeText(this,"onDestroy()",Toast.LENGTH_SHORT).show();
    }
}
