package org.insbaixcamp.aauf1;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements AdapterView.OnItemClickListener, View.OnClickListener {
    //Categoria i subcategoria que trobem en el JSON
    private static final String CATEGORIA = new String("android");
    private static final String SUBCATEGORIA1 = new String("ver");
    private static final String SUBCATEGORIA2 = new String("name");
    private static final String SUBCATEGORIA3 = new String("api");
    //indiquem la llista dels elements a mostrar
    ListView lvLlista;
    //creem un nou objecte ArrayList per inserir les dades
    ArrayList<HashMap<String,String>> arrayListAndroid = new ArrayList<HashMap<String,String>>();
    // Progress bar
    ProgressBar pbLoad;
    TextView tvLoading;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(this);
        lvLlista=findViewById(R.id.lvLlista);

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        //noinspection SimplifiableIfStatement
        if (id == R.id.acAbout) {
            //intent de About
            Intent about = new Intent(this,About.class);
            startActivity(about);
        }

        return super.onOptionsItemSelected(item);
    }

    public void loadList(){
        // mostrem progresBar
        showBar();
        // Instantiate the RequestQueue.
        RequestQueue queue = Volley.newRequestQueue(this);
        String url ="http://insbaixcamp.org/android/versions.json";

        // Request a string response from the provided URL.
        StringRequest stringRequest = new StringRequest(Request.Method.GET, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                       //delay(3,response);
                        onPostExecute(response);
                    }
                }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
               Toast.makeText(MainActivity.this,R.string.ntErrorRequest,Toast.LENGTH_SHORT).show();
               hiddenBar();

            }
        });

        // Add the request to the RequestQueue.
        queue.add(stringRequest);
    }

    protected void onPostExecute(String stream){
        HashMap<String,String> hashMap;
        Log.d("Entrada onPostExecute",stream);
        //creem un nou objecte ArrayList per inserir les dades
        //ArrayList<HashMap<String,String>> arrayListAndroid = new ArrayList<HashMap<String,String>>();
        //Processem les Dades del JSON
        if(stream!=null){
            //Toast.makeText(MainActivity.this,stream.toString(),Toast.LENGTH_LONG).show();
            //obtenim totes les dades via HTTP com JSONObject
            try {
                JSONObject jsonObject = new JSONObject(stream);
                // desem en JSONArray
                JSONArray jsonArray = jsonObject.getJSONArray(CATEGORIA);
                for(int i =0; i< jsonArray.length(); i++){
                    JSONObject jsonObjectLine = jsonArray.getJSONObject(i);
                    //desem els JSON en una variable
                    String ver = jsonObjectLine.getString(SUBCATEGORIA1);
                    String name = jsonObjectLine.getString(SUBCATEGORIA2);
                    String api = jsonObjectLine.getString(SUBCATEGORIA3);
                    //afecing a la clau-valor amb un objetce HasMap
                    hashMap = new HashMap<String, String>();
                    hashMap.put(SUBCATEGORIA1,ver);
                    hashMap.put(SUBCATEGORIA2,name);
                    hashMap.put(SUBCATEGORIA3,api);
                    arrayListAndroid.add(hashMap);
                    //mostrem per pantalla els elemenst dels arrayListAndroid
                    ListAdapter adapter = new SimpleAdapter(
                            MainActivity.this,
                            arrayListAndroid,
                            R.layout.llista,
                            new String[]{SUBCATEGORIA1,SUBCATEGORIA2,SUBCATEGORIA3},
                            new int[]{R.id.tvVersio,R.id.tvNom,R.id.tvApi});
                    lvLlista.setAdapter(adapter);
                    lvLlista.setOnItemClickListener(this);
                    hiddenBar();
                }
            } catch (JSONException e) {
                //control del JSON
                e.printStackTrace();
            }

        }
    }
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Log.i("Position to array", String.valueOf(arrayListAndroid.get(position)));
        // mostrem per pantall l'element clicat
           /* Toast.makeText(MainActivity.this,
                    arrayListAndroid.get(position).get(SUBCATEGORIA1.replace(" ", ""))+
                    " -> "+arrayListAndroid.get(position).get(SUBCATEGORIA2)+
                    " -> "+arrayListAndroid.get(position).get(SUBCATEGORIA3),
                    Toast.LENGTH_LONG).show();*/
        Intent intent = new Intent(MainActivity.this,MostraImatge.class);
        intent.putExtra("ver",arrayListAndroid.get(position).get(SUBCATEGORIA1));
        intent.putExtra("name",arrayListAndroid.get(position).get(SUBCATEGORIA2));
        intent.putExtra("API",arrayListAndroid.get(position).get(SUBCATEGORIA3));
        startActivity(intent);
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }

    @Override
    public void onClick(View view) {
        if (!hiHaConexio()) {
            Snackbar.make(view,R.string.sbnoconnection, Snackbar.LENGTH_LONG).show();
        }else{
            loadList();
        }

    }
    private boolean hiHaConexio(){
        ConnectivityManager connectivityManager =(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
            return (true);

        }else{
            return (false);
        }
    }
    private void showBar(){
        pbLoad=findViewById(R.id.pbLoad);
        pbLoad.setVisibility(View.VISIBLE);
        tvLoading=findViewById(R.id.tvLoading);
        tvLoading.setVisibility(View.VISIBLE);
    }
    private void hiddenBar(){
        pbLoad=findViewById(R.id.pbLoad);
        pbLoad.setVisibility(View.INVISIBLE);
        tvLoading=findViewById(R.id.tvLoading);
        tvLoading.setVisibility(View.INVISIBLE);
    }
    public void delay(int seconds, final String response){
        // retartd en la ocultacio
        CountDownTimer delay3 = new CountDownTimer(seconds*1000,1000){
            @Override
            public void onTick(long millisUntilFinished) {
                showBar();
                onPostExecute(response);
            }

            @Override
            public void onFinish() {
                hiddenBar();
                //ocultem barra
                hiddenBar();
            }
        };
        delay3.start();
    }
}
