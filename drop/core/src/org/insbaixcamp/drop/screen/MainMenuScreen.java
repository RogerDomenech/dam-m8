package org.insbaixcamp.drop.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.freetype.FreeType;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;

import org.insbaixcamp.drop.MyGdxGame;

public class MainMenuScreen implements Screen {

    private final OrthographicCamera camera;
    private MyGdxGame game;


    public MainMenuScreen(MyGdxGame myGdxGame) {
        this.game=myGdxGame;
        camera = new OrthographicCamera();
        camera.setToOrtho(false,800,480);

    }

    @Override
    public void show() {
        // quan es carrega

    }

    @Override
    public void render(float delta) {
        // bucle de la activitat
        //inici el joc
        game.batch.begin();
        //Textod'inici

        game.font.draw(game.batch,"Welcome to Drop!!",100,150);
        game.font.draw(game.batch,"Tap anywhere to begin!",100,100);
        //background
        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        //cambiar pantalla tocant anywhere
        if(Gdx.input.isTouched()){
            game.setScreen(new GameScreen(game));
            // elimiem la pantalla
            this.dispose();
        }
        //fi del joc
        game.batch.end();
    }

    @Override
    public void resize(int width, int height) {
        // rescalara pantalla

    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {
    // minimitzar
    }

    @Override
    public void dispose() {
    // eliminiar
    }

}
