package org.insbaixcamp.drop.screen;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.StretchViewport;

import org.insbaixcamp.drop.MyGdxGame;

import java.util.Iterator;

public class GameScreen implements Screen {

    private final MyGdxGame game;
    private Texture bucketImage;
    private Rectangle bucket;
    private Music rainMusic;
    private Sound dropSound;
    private OrthographicCamera camera;
    private Texture dropImage;
    private Array<Rectangle> raindrops;
    private long lastDropTime;
    private StretchViewport viewport;


    public GameScreen(MyGdxGame game) {
        this.game = game;
    }

    @Override
    public void show() {
        // textura
        bucketImage = new Texture(Gdx.files.internal("img/bucket.png"));
        // colisions
        bucket = new Rectangle();
        bucket.x = 800 / 2 - 64 / 2;
        bucket.y = 20;
        bucket.width = 64;
        bucket.height = 64;
        //musica
        rainMusic = Gdx.audio.newMusic(Gdx.files.internal("music/rain.mp3"));
        rainMusic.play();
        //so
        dropSound = Gdx.audio.newSound(Gdx.files.internal("sound/drop.wav"));
        //camera
        camera= new OrthographicCamera();
        viewport = new StretchViewport(800,480,camera);
        viewport.apply();


        //Escena


        // gota
        dropImage = new Texture((Gdx.files.internal("img/droplet.png")));
        raindrops = new Array<Rectangle>();


    }

    @Override
    public void render(float delta) {

        Gdx.gl.glClearColor(0, 0, 0.2f, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        camera.update();
        // printa
        game.batch.begin();
        game.batch.draw(bucketImage, bucket.getX(), bucket.getY());
        for (Rectangle rainDrop:raindrops) {
            game.batch.draw(dropImage,rainDrop.x,rainDrop.y);
        }
        //update score
        game.font.draw(game.batch,"Score: "+game.score,100,15);
        game.batch.end();

        // move touch
        if(Gdx.input.isTouched()) {
            //vector de 3D
            Vector3 touchPos = new Vector3();
            //Definicio de les coordenades del Vectorm segons el input
            touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
            //Centrem la camera a la possicio del touch
            Gdx.app.log("touchX",String.valueOf(touchPos.x));
            camera.unproject(touchPos);
            // centrem la X del bucket al touch -> aixo el mou
            bucket.x = touchPos.x - (64 / 2);
        }
        // move keyboard
        //getDeltaTime es la data del ultima entrada.
        if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) bucket.x -= 200 * delta;
        if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) bucket.x += 200 * delta;
        // limits de pantalla
        if(bucket.x <0){bucket.x=0;}
        if(bucket.x > 800-64){bucket.x = 800-64;}

        // generar gotes
        if(TimeUtils.nanoTime()-lastDropTime > 1000000000){spawnRaindrops();}

        // movem les gotes
        for (Iterator<Rectangle> iter = raindrops.iterator(); iter.hasNext();) {
            Rectangle rainDrop = iter.next();
            rainDrop.y -= 200 * delta;

            //colixio
            if (rainDrop.overlaps(bucket)) {
                iter.remove();
                dropSound.play();
                game.score+=1;
            }
            // eliminem les gotes de fora de la pantalla
            if (rainDrop.y + 64 < 0) {
                iter.remove();
                game.score-=1;
            }
        }
    }

    private void spawnRaindrops(){
        Rectangle raindrop = new Rectangle();
        raindrop.width=64;
        raindrop.height=64;
        raindrop.y = 480;
        raindrop.x = MathUtils.random(0,800-64);
        raindrops.add(raindrop);
        lastDropTime = TimeUtils.nanoTime();
    }
    @Override
    public void resize(int width, int height) {
        viewport.update(width,height);
        camera.position.set(camera.viewportWidth/2,camera.viewportHeight/2,0);
        //camera.setToOrtho(false,camera.viewportWidth,camera.viewportHeight);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {
        game.batch.dispose();
        dropSound.dispose();
        dropImage.dispose();
        bucketImage.dispose();
        rainMusic.dispose();
    }
}
