package org.insbaixcamp.drop;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import java.util.Iterator;


public class MyGdxGame_bak extends ApplicationAdapter {
	SpriteBatch batch;
	Texture img;
	private Texture bucketImage;
	private Music rainMusic;
	private OrthographicCamera camera;
	private Rectangle bucket;
	private Texture dropImage;
	private Array<Rectangle> raindrops;
	private long lastDropTime;
	private Sound dropSound;

	@Override
	public void create () {

		//img = new Texture("badlogic.jpg");
		// textura
		bucketImage = new Texture(Gdx.files.internal("bucket.png"));
		// colisions
		bucket = new Rectangle();
		bucket.x = 800 / 2 - 64 / 2;
		bucket.y = 20;
		bucket.width = 64;
		bucket.height = 64;
		//musica
		rainMusic = Gdx.audio.newMusic(Gdx.files.internal("rain.mp3"));
		rainMusic.play();
		//so
		dropSound = Gdx.audio.newSound(Gdx.files.internal("drop.wav"));
		//camera
		camera= new OrthographicCamera();
		camera.setToOrtho(false,800,480);

		//Escena
		batch = new SpriteBatch();

		// gota
		dropImage = new Texture((Gdx.files.internal("droplet.png")));
		raindrops = new Array<Rectangle>();
	}

	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0.2f, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		camera.update();
		// printa
		batch.begin();
		batch.draw(bucketImage, bucket.getX(), bucket.getY());
		for (Rectangle rainDrop:raindrops) {
			batch.draw(dropImage,rainDrop.x,rainDrop.y);
		}
		batch.end();

		// move touch
		if(Gdx.input.isTouched()) {
			//vector de 3D
			Vector3 touchPos = new Vector3();
			//Definicio de les coordenades del Vectorm segons el input
			touchPos.set(Gdx.input.getX(), Gdx.input.getY(), 0);
			//Centrem la camera a la possicio del touch
			camera.unproject(touchPos);
			// centrem la X del bucket al touch -> aixo el mou
			bucket.x = touchPos.x - 64 / 2;
		}
		// move keyboard
		//getDeltaTime es la data del ultima entrada.
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)) bucket.x -= 200 * Gdx.graphics.getDeltaTime();
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)) bucket.x += 200 * Gdx.graphics.getDeltaTime();
		// limits de pantalla
		if(bucket.x <0){bucket.x=0;}
		if(bucket.x > 800-64){bucket.x = 800-64;}

		// generar gotes
		if(TimeUtils.nanoTime()-lastDropTime > 1000000000){spawnRaindrops();}

		// movem les gotes
		for (Iterator<Rectangle> iter=raindrops.iterator();iter.hasNext();) {
			Rectangle rainDrop = iter.next();
			rainDrop.y -= 200 * Gdx.graphics.getDeltaTime();

			//colixio
			if (rainDrop.overlaps(bucket)) {
				iter.remove();
				dropSound.play();
			}
			// eliminem les gotes de fora de la pantalla
			if (rainDrop.y + 64 < 0) {
				iter.remove();
			}
		}
	}
	
	@Override
	public void dispose () {
		batch.dispose();
		dropSound.dispose();
		dropImage.dispose();
		bucketImage.dispose();
		rainMusic.dispose();
	}
	private void spawnRaindrops(){
		Rectangle raindrop = new Rectangle();
		raindrop.width=64;
		raindrop.height=64;
		raindrop.y = 480;
		raindrop.x = MathUtils.random(0,800-64);
		raindrops.add(raindrop);
		lastDropTime = TimeUtils.nanoTime();
	}
}
