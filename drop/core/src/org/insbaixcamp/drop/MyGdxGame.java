package org.insbaixcamp.drop;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.freetype.FreeTypeFontGenerator;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.TimeUtils;

import org.insbaixcamp.drop.screen.MainMenuScreen;

import java.util.Iterator;


public class MyGdxGame extends Game {
	public SpriteBatch batch;
	public BitmapFont font;
	public BitmapFont title;
	public Integer score;

	@Override
	public void create () {
		//Menu
		this.setScreen(new MainMenuScreen(this));
		//Escena
		batch = new SpriteBatch();
		//Font
		font = new BitmapFont();
		title = this.title();
		//puntuacio
		score = 0;


	}

	@Override
	public void render () {
		super.render();
	}
	
	@Override
	public void dispose () {
		batch.dispose();

	}
	//fonts
	public BitmapFont title(){
		FreeTypeFontGenerator generator = new FreeTypeFontGenerator(Gdx.files.internal("font/Montserrat-Bold.ttf"));
		FreeTypeFontGenerator.FreeTypeFontParameter parameter = new FreeTypeFontGenerator.FreeTypeFontParameter();
		parameter.size = 18;
		parameter.color = Color.WHITE;
		BitmapFont fontTitle = generator.generateFont(parameter); // font size 18 pixels
		generator.dispose(); // don't forget to dispose to avoid memory leaks!
		return fontTitle;
	}
}
