package org.insbaixcamp.drop.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import org.insbaixcamp.drop.MyGdxGame;

public class DesktopLauncher {
	public static void main (String[] arg) {
		LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
		config.title="Drop";
		config.height=480;
		config.width=800;

		new LwjglApplication(new MyGdxGame(), config);
	}
}
