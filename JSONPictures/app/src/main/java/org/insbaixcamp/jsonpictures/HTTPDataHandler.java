package org.insbaixcamp.jsonpictures;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

/**
 * Classe per maniplar les dades JSON
 */
public class HTTPDataHandler {
    //metode pre retornar un String amb les dades de la URL
  public String getHTTPData(String urlString){
      // String contenidor
      String stream = new String();
      // urlConnection contenidor
      HttpURLConnection urlConnection = null;

      try {
          URL url = new URL(urlString);
          urlConnection = (HttpURLConnection) url.openConnection();
          InputStream inputStream = new BufferedInputStream(urlConnection.getInputStream());
          BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
          StringBuilder sb = new StringBuilder();
          String line;
          while ((line = bufferedReader.readLine())!=null){
              sb.append(line);
          }
          stream=sb.toString();
      } catch (MalformedURLException e) {
          //Error del format de la URL
          e.printStackTrace();
      } catch (IOException e) {
          //Error del conexio
          e.printStackTrace();
      } finally {
          //desconecio
          urlConnection.disconnect();
      }
    return stream;
  }
}

