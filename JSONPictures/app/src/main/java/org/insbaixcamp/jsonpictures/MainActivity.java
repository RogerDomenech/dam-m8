package org.insbaixcamp.jsonpictures;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.Network;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.os.Process;
import android.util.Log;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    //Categoria i subcategoria que trobem en el JSON
    private static final String CATEGORIA = new String("android");
    private static final String SUBCATEGORIA1 = new String("ver");
    private static final String SUBCATEGORIA2 = new String("name");
    private static final String SUBCATEGORIA3 = new String("api");
    //indiquem la llista dels elements a mostrar
    ListView lvLlista;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton bGet = findViewById(R.id.bGet);
        bGet.setOnClickListener(this);

        lvLlista=findViewById(R.id.lvLlista);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View view) {
        if (!hiHaConexio()) {
            Snackbar.make(view, "No Tens conexio a Internet", Snackbar.LENGTH_LONG)
                    .setAction("Action", null).show();
        }else{
           String urlString = "http://insbaixcamp.org/android/versions.json";
           new ProcessJSON().execute(urlString);
        }

    }
    private boolean hiHaConexio(){
        ConnectivityManager connectivityManager =(ConnectivityManager)getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = connectivityManager.getActiveNetworkInfo();
        if (activeNetwork != null && activeNetwork.isConnectedOrConnecting()){
            return (true);

        }else{
            return (false);
        }
    }
    private class ProcessJSON extends AsyncTask<String,Void,String> implements AdapterView.OnItemClickListener{

        //creem un nou objecte ArrayList per inserir les dades
        ArrayList<HashMap<String,String>> arrayListAndroid = new ArrayList<HashMap<String,String>>();

        @Override
        protected String doInBackground(String... strings) {
            String urlString = strings[0];
            HTTPDataHandler httpDataHandler = new HTTPDataHandler();
            String result= httpDataHandler.getHTTPData(urlString);
            //Log.i("JSON",result);
            return result;
        }

        @Override
        protected void onPostExecute(String stream){
            HashMap<String,String>hashMap;
            //creem un nou objecte ArrayList per inserir les dades
            //ArrayList<HashMap<String,String>> arrayListAndroid = new ArrayList<HashMap<String,String>>();
            //Processem les Dades del JSON
            if(stream!=null){
                //Toast.makeText(MainActivity.this,stream.toString(),Toast.LENGTH_LONG).show();
                //obtenim totes les dades via HTTP com JSONObject
                try {
                    JSONObject jsonObject = new JSONObject(stream);
                    // desem en JSONArray
                    JSONArray jsonArray = jsonObject.getJSONArray(CATEGORIA);
                    for(int i =0; i< jsonArray.length(); i++){
                        JSONObject jsonObjectLine = jsonArray.getJSONObject(i);
                        //desem els JSON en una variable
                        String ver = jsonObjectLine.getString(SUBCATEGORIA1);
                        String name = jsonObjectLine.getString(SUBCATEGORIA2);
                        String api = jsonObjectLine.getString(SUBCATEGORIA3);
                        //afecing a la clau-valor amb un objetce HasMap
                        hashMap = new HashMap<String, String>();
                        hashMap.put(SUBCATEGORIA1,ver);
                        hashMap.put(SUBCATEGORIA2,name);
                        hashMap.put(SUBCATEGORIA3,api);
                        arrayListAndroid.add(hashMap);
                        //mostrem per pantalla els elemenst dels arrayListAndroid
                        ListAdapter adapter = new SimpleAdapter(
                                MainActivity.this,
                                arrayListAndroid,
                                R.layout.llista,
                                new String[]{SUBCATEGORIA1,SUBCATEGORIA2,SUBCATEGORIA3},
                                new int[]{R.id.tvVersio,R.id.tvNom,R.id.tvApi});
                        lvLlista.setAdapter(adapter);
                        lvLlista.setOnItemClickListener(this);
                    }
                } catch (JSONException e) {
                    //control del JSON
                    e.printStackTrace();
                }

            }
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            Log.i("Position to array", String.valueOf(arrayListAndroid.get(position)));
            // mostrem per pantall l'element clicat
           /* Toast.makeText(MainActivity.this,
                    arrayListAndroid.get(position).get(SUBCATEGORIA1.replace(" ", ""))+
                    " -> "+arrayListAndroid.get(position).get(SUBCATEGORIA2)+
                    " -> "+arrayListAndroid.get(position).get(SUBCATEGORIA3),
                    Toast.LENGTH_LONG).show();*/
            Intent intent = new Intent(MainActivity.this,MostraImatge.class);
            intent.putExtra("ver",arrayListAndroid.get(position).get(SUBCATEGORIA1));
            intent.putExtra("name",arrayListAndroid.get(position).get(SUBCATEGORIA2));
            intent.putExtra("API",arrayListAndroid.get(position).get(SUBCATEGORIA3));
            startActivity(intent);
        }
    }
}
