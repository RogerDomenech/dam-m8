package org.insbaixcamp.jsonpictures;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.toolbox.ImageRequest;
import com.android.volley.toolbox.Volley;


/**
 * Classe per mostr el logo de cada versio d'android
 */

public class MostraImatge extends AppCompatActivity implements Response.Listener<Bitmap> {
    // image View
    ImageView ivLogo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mostra_imatge);
        // variables contenidors
        String ver;
        String name;
        String api;
        // Elements del Layout
        TextView tvVersio;
        TextView tvApi;
        //recollim el intent
        Intent intent = getIntent();
        ver = intent.getStringExtra("ver");
        name = intent.getStringExtra("name");
        api = intent.getStringExtra("API");
        // Creem url
        String url = "https://insbaixcamp.org/android/"+name.toLowerCase()+".jpg";
        // Assignacio elements del Layaout
        tvVersio=findViewById(R.id.tvVersio);
        tvApi=findViewById(R.id.tvApi);
        ivLogo=findViewById(R.id.ivLogo);
        // assignar valors a elements
        tvVersio.setText(getString(R.string.versio)+" "+ver);
        tvApi.setText(getString(R.string.api)+" "+api);
        // assingem valor de url a la imatge
        ImageRequest imageRequest= new ImageRequest(url,
                this,
                0,
                0,
                ImageView.ScaleType.CENTER_INSIDE,
                null,
                null);
        RequestQueue requestQueue= Volley.newRequestQueue(this);
        requestQueue.add(imageRequest);
    }

    // metode per el listener del imagRequest
    @Override
    public void onResponse(Bitmap response) {
        ivLogo.setImageBitmap(response);
    }
}
