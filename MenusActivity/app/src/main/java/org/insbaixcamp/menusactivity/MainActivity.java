package org.insbaixcamp.menusactivity;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    //Opcions del menu
    //Subsmenus marcats com numOpcio  OpcioSubmenu
    private final int MENU_OP1 = R.id.mOpcio1;
    private final int MENU_OP2 = R.id.mOpcio2;
    private final int MENU_OP3 = R.id.mOpcio3;
    private final int MENU_OP31 = R.id.mOpcio31;
    private final int MENU_OP32= R.id.mOpcio32;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

    }
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Forma 1: utilitzant el menu_main.xml
        // Inflem el menu afegint items dins del codi xml.
         getMenuInflater().inflate(R.menu.menu_main, menu);

        // Forma 2: definim el menú amb sentències Java
        /*SubMenu subMenu;

        // L'Objecte "menu" no permet mostrar icones,
        // per decisició de la guia d'estils d'Android.
        // Sintaxi:
        // add(int groupId, int itemId, int order, CharSequence title)
        menu.add(Menu.NONE, MENU_OP1, Menu.NONE,
                "Opció 1 sense internacionalització")
                .setIcon(R.drawable.ic_launcher_foreground);
        menu.add(Menu.NONE, MENU_OP2, Menu.NONE,
                R.string.opcio_2);
        subMenu = menu.addSubMenu(Menu.NONE, MENU_OP3,
                Menu.NONE, R.string.opcio_3);
        // L'Objecte "submenu" sí permet mostrar icones.
        subMenu.add(Menu.NONE, MENU_OP31, Menu.NONE, R.string.opcio_31)
                .setIcon(R.drawable.ic_launcher_background);
        subMenu.add(Menu.NONE, MENU_OP32, Menu.NONE, R.string.opcio_32)
                .setIcon(R.drawable.ic_launcher_foreground);
*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        //Toast.makeText(getBaseContext(),"test id "+id,Toast.LENGTH_SHORT).show();
        if (id == MENU_OP1) {
            Toast.makeText(getBaseContext(),R.string.opcio_1, Toast.LENGTH_SHORT).show();
        } else if (id == MENU_OP2) {
            Toast.makeText(getBaseContext(),R.string.opcio_2, Toast.LENGTH_SHORT).show();
        }else if (id == MENU_OP3) {
            Toast.makeText(getBaseContext(),R.string.opcio_3, Toast.LENGTH_SHORT).show();
        } else if (id == MENU_OP31) {
            Toast.makeText(getBaseContext(),R.string.opcio_31, Toast.LENGTH_SHORT).show();
        } else if (id == MENU_OP32) {
            Toast.makeText(getBaseContext(),R.string.opcio_32, Toast.LENGTH_SHORT).show();
        }


//        // Si utilitzo el menu_main.xml, l'id és diferent:
//        if (id == R.id.action_settings) {
//            Toast.makeText(getBaseContext(), R.string.action_settings, Toast.LENGTH_SHORT).show();
//        }

        return super.onOptionsItemSelected(item);
    }
}
