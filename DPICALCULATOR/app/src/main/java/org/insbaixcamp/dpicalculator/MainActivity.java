package org.insbaixcamp.dpicalculator;

import android.content.Context;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.view.Display;
import android.view.Surface;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    // creem els objectes que repesenten el control de les vistes
    //Entradas
    EditText etResolucioVertical;
    EditText etResolucioHorizontal;
    EditText etDiagonal;

    //Sortida
    TextView tvResultatResolucioVertical;
    TextView tvResultatResolucioHorizontal;
    TextView tvResultatDiagonal;

    // resultats
    TextView tvResultatDPI;
    TextView tvResultatDP;

    //display
    Display display;

    Pantalla pantalla;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Sortides
        tvResultatResolucioVertical=findViewById(R.id.tvResultatResolucioVertical);
        tvResultatResolucioHorizontal = findViewById(R.id.tvResultatResolucioHorizontal);
        tvResultatDiagonal = findViewById(R.id.tvResultatDiagonal);
        tvResultatDPI = findViewById(R.id.tvResultatDPI);
        tvResultatDP = findViewById(R.id.tvResultatDP);

        //Entradas
        etResolucioVertical = findViewById(R.id.etResolucioVertical);
        etResolucioHorizontal = findViewById(R.id.etResolucioHorizontal);
        etDiagonal = findViewById(R.id.etDiagonal);


        //display
        display = ((WindowManager)getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay();
        //assiganacio en horizontal
        if((display.getRotation()== Surface.ROTATION_90)||(display.getRotation()== Surface.ROTATION_270)){
            // si existeix valors anteriors.
            if(savedInstanceState!= null){
                tvResultatDPI.setText(savedInstanceState.getString("tvResultatDPI"));
                tvResultatResolucioVertical.setText(savedInstanceState.getString("tvResultatResolucioVertical"));
                tvResultatResolucioHorizontal.setText(savedInstanceState.getString("tvResultatResolucioHorizontal"));
                tvResultatDiagonal.setText(savedInstanceState.getString("tvResultatResolucioHorizontal"));
            }
            //calcular
            carregaHorizontal();
        }
        //assignacio en vertical
        if((display.getRotation()== Surface.ROTATION_0)||(display.getRotation()== Surface.ROTATION_180)){
            // si existeix valors anteriors.
            if(savedInstanceState!= null){
                //passar els et de String a Integer
                etDiagonal.setText(savedInstanceState.getString("tvResultatDiagonal"));
                etResolucioVertical.setText(savedInstanceState.getString("tvResultatResolucioVertical"));
                etResolucioHorizontal.setText(savedInstanceState.getString("tvResultatResolucioHorizontal"));
                tvResultatDPI.setText(savedInstanceState.getString("tvResultatDPI"));
            }
        }

    }
    @Override
    protected void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);

        if(tvResultatDPI!=null) {
            outState.putString("tvResultatDPI", String.valueOf(tvResultatDPI.getText()));
        }
        if(tvResultatResolucioVertical!=null) {
            outState.putString("tvResultatResolucioVertical", String.valueOf(tvResultatResolucioVertical.getText()));
        }
        if(tvResultatResolucioHorizontal!=null) {
            outState.putString("tvResultatResolucioHorizontal", String.valueOf(tvResultatResolucioHorizontal.getText()));
        }
        if(tvResultatDiagonal!=null) {
            outState.putString("tvResultatDiagonal", String.valueOf(tvResultatDiagonal.getText()));
        }
    }

    public void carregaHorizontal(){
        //Cridem la classe DisplayMetrics per mesurar la pantalla

        DisplayMetrics displayMetrics = new DisplayMetrics();
        //delacara les variables
        int height;
        int width;
        int dpi;
        //agafem informacio de la pantalla
        //¿? incluir el display al crear pantalla i cridar els elements amb gets
        display.getMetrics(displayMetrics);
        height= displayMetrics.heightPixels;
        width= displayMetrics.widthPixels;
        dpi = displayMetrics.densityDpi;
        // assignar valors a variables
        tvResultatResolucioHorizontal.setText(String.valueOf(width));
        tvResultatResolucioVertical.setText(String.valueOf(height));
        tvResultatDPI.setText(String.valueOf(dpi));
        //calcula la hipotenusa
        pantalla= new Pantalla(width,height,dpi);
        tvResultatDiagonal.setText(String.format("%.2f",pantalla.getDiagonal()));
        // calcular DP
        tvResultatDP.setText(String.valueOf(pantalla.getDp(display)));

    }


    public void calculaDPI(View view){
        //Per calcular la densitat de la pantalla (dpi)
        // dpi = ArrelQuadarada((resHor^2 + (resVer)^2 /diagonal
        // comprovem si els camps estan plens o buits
        Boolean checketResolucioVertical = StringHelper.stringIsNullOrEmpty(etResolucioVertical.getText().toString());
        Boolean checketResolucioHorizontal = StringHelper.stringIsNullOrEmpty(etResolucioHorizontal.getText().toString());
        Boolean checketDiagonal = StringHelper.stringIsNullOrEmpty(etDiagonal.getText().toString());

        //si tots els camps contenen text
        if(!(checketResolucioVertical&&checketResolucioHorizontal&&checketDiagonal)) {
            int resolucioHorizontal = Integer.parseInt(etResolucioHorizontal.getText().toString());
            int resolucioVertical = Integer.parseInt(etResolucioVertical.getText().toString());
            double diagonal = Double.parseDouble(etDiagonal.getText().toString());
            // calcular
            pantalla = new Pantalla(resolucioVertical,resolucioHorizontal,diagonal);
            // calcular DPI
            int resultatDPI = pantalla.getDpi();
            //assignar valor de resultat al tvResultatDPI
            tvResultatDPI.setText(String.format("%.2f",String.valueOf(resultatDPI)));
            // calcular DP
           // int resultatDP = pantalla.getDp();
            //assignar valor de resultat al tvResultatDP
//            // si el etResolucioVertical esta buit o null fa focus al etResolucioVertical
        }else if(checketResolucioVertical){
            etResolucioVertical.requestFocus();
            // si el etResolucioHorizontal esta buit o null fa focus al etResolucioHorizontal
        }else if(checketResolucioHorizontal){
            etResolucioHorizontal.requestFocus();
            // si el etDiagonal esta buit o null fa focus al etDiagonal
        }else{
            etDiagonal.requestFocus();
        }
    }
}
