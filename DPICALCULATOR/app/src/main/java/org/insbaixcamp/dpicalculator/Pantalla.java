package org.insbaixcamp.dpicalculator;

import android.content.Context;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import androidx.appcompat.app.AppCompatActivity;

public class Pantalla extends AppCompatActivity {
    private int resolucioHorizontal;
    private int resolucioVertical;
    private double diagonal;
    private int dpi;


    /**
     * Contructor de Pantalla
     * @param resolucioVertical
     * @param resolucioHorizontal
     * @param diagonal
     */
    public Pantalla(int resolucioVertical, int resolucioHorizontal, double diagonal){
        this.resolucioHorizontal = resolucioHorizontal;
        this.resolucioVertical = resolucioVertical;
        this.diagonal = diagonal;
        int dpi;
        // dins del contructor calculem el DPI
        if(getDiagonal() == 0.0){
            dpi=0;
        }else{
            double resultat = Math.round((Math.sqrt(Math.pow(this.resolucioHorizontal, 2D) + Math.pow(this.resolucioVertical, 2D)) / this.diagonal));
            dpi = (int)(resultat);
        }
        this.dpi =dpi;
    }


    public Pantalla(int resolucioVertical, int resolucioHorizontal, int dpi){
        this.resolucioHorizontal = resolucioHorizontal;
        this.resolucioVertical = resolucioVertical;
        this.dpi = dpi;
        double diagonal;
        // dins del constructor calculem la diagonal
        if(getDpi()==0){
            diagonal =0.0;
        }else{
            diagonal = Math.sqrt(Math.pow(this.resolucioVertical,2D)+Math.pow(this.resolucioHorizontal,2D))/this.dpi;
        }
        this.diagonal=diagonal;
    }
    /**
     * Calucla el DPI
     * @return
     */
    public int getDpi(){
        return this.dpi;
    }
    public double getDiagonal(){
       return this.diagonal;
    }

    public int getResolucioVertical(){
        return this.resolucioVertical;
    }
    public int getResolucioHorizontal(){
        return this.resolucioHorizontal;
    }

    //dp =((displayMetrics.heightPixels *160) / displayMetrics.densitatDpi);
    public int getDp(Display display){
        //agafem informacio de la pantalla
        DisplayMetrics displayMetrics = new DisplayMetrics();
        display.getMetrics(displayMetrics);
        int dp = ((displayMetrics.heightPixels *160)/displayMetrics.densityDpi);
        Log.i("HEIGHTPIXELS",(String.valueOf(displayMetrics.heightPixels)));
        Log.i("Dpi",(String.valueOf(displayMetrics.densityDpi)));

       return dp;
    }
}
